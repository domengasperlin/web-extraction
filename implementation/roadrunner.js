const {DOMBuilder, DOMIterator, TagUtils, Wrapper} = require('./DOMHelpers');
const fs = require('fs');

let validFolders = ['overstock.com', 'rtvslo.si', 'theverge.com'];
let folder = process.argv[2];
if(!validFolders.includes(folder)) {
    console.log('Invalid folder name provided.');
    process.exit(1);
}

let data;
try {
    data = fs.readdirSync(`../input/${folder}`);
} catch (e) {
    console.log('Something went wrong opening the folder. Check if the selected folder exists.');
    process.exit(1);
}

let htmlFiles = [];
let htmlNames = [];
data.forEach(file => {
    let found = file.match(/.html/);
    if(found) {
        htmlFiles.push(fs.readFileSync(`../input//${folder}/${file}`).toString());
        htmlNames.push(file);
    }
});

let firstFileDOM = new DOMBuilder(htmlFiles[0]);
let secondFileDOM = new DOMBuilder(htmlFiles[1]);
let firstFileIterator = new DOMIterator(firstFileDOM.rootTag);
let secondFileIterator = new DOMIterator(secondFileDOM.rootTag);
let mainWrapper = new Wrapper();

function p (iterator1, iterator2, wrapper) {
    let next = false;
    while(true) {
        if(next) {
            iterator1.next();
            iterator2.next();
        } else {
            next = true;
        }
        let tag1 = iterator1.value();
        let tag2 = iterator2.value();

        if(tag1 == null || tag2 == null) { 
            break;
        }
        if(TagUtils.equal(tag1, tag2)) {
            continue;
        } 
        if(TagUtils.closelyEqual(tag1, tag2)) {
            TagUtils.findDifferences(tag1, tag2, wrapper);
            continue;
        }
        let optional = checkIfOptional(tag1, tag2, iterator1, iterator2);
        if(optional) {
            if(optional.type == 1) {
                for(let i = 0; i<optional.skipped; i++) {
                    iterator1.next();
                }
            } else if(optional.type == 2) {
                for(let i = 0; i<optional.skipped; i++) {
                    iterator2.next();
                }
            }
            next = false;
        } else {
            //we need to go deeper to check for differences
            p(iterator1.goDeeper(), iterator2.goDeeper(), wrapper);
        }
    }
}

function checkIfOptional(tag1, tag2, iterator1, iterator2) {
    let status = false;
    iterator1.save();
    iterator2.save();
    iterator1.next();
    iterator2.next();

    let count1 = 0;
    while(!status && true) {
        count1++;
        let value = iterator1.value();
        if(!value) {
            break;
        }
        if(TagUtils.equal(tag2, value) || TagUtils.closelyEqual(tag2, value)) {
            status = {
                type: 1,
                skipped: count1
            };
        } 
        iterator1.next();
    }

    let count2 = 0;
    while(!status && true) {
        count2++;
        let value = iterator2.value();
        if(!value) {
            break;
        }
        if(TagUtils.equal(tag1, value) || TagUtils.closelyEqual(tag1, value)) {
            status = {
                type: 2,
                skipped: count2
            };
        } 
        iterator2.next();
    }

    iterator1.restore();
    iterator2.restore();
    return status;
}

p(firstFileIterator, secondFileIterator, mainWrapper);

let items = [];
if(folder === 'rtvslo.si') {
    let authorAndTime1 = mainWrapper.queryAttr(0, 'class', 'author-timestamp').split('|');
    let authorAndTime2 = mainWrapper.queryAttr(1, 'class', 'author-timestamp').split('|');
    let publishedTime1 = authorAndTime1[1].split('ob');
    let publishedTime2 = authorAndTime2[1].split('ob');
    items.push({
        title: mainWrapper.queryTag(0, 'h1'),
        subtitle: mainWrapper.queryAttr(0, 'class', 'subtitle'),
        author: authorAndTime1[0].trim(), 
        publishedTime: {
            date: publishedTime1[0].trim(),
            hour: publishedTime1[1].trim()
        },
        lead: mainWrapper.queryTag(0, 'header'),
        content: mainWrapper.queryTag(0, 'article'),
        sourceFile: htmlNames[0]
    });
    items.push({
        title: mainWrapper.queryTag(1, 'h1'),
        subtitle: mainWrapper.queryAttr(1, 'class', 'subtitle'),
        author: authorAndTime2[0].trim(), 
        publishedTime: {
            date: publishedTime2[0].trim(),
            hour: publishedTime2[1].trim()
        },
        lead: mainWrapper.queryTag(1, 'header'),
        content: mainWrapper.queryTag(1, 'article'),
        sourceFile: htmlNames[1]
    });
} else if(folder == 'theverge.com') {
    items.push({
        title: mainWrapper.queryTag(0, 'h1'),
        subtitle: mainWrapper.queryTag(0, 'h2'),
        author: mainWrapper.queryAttr(0, 'href', /https:\/\/www.theverge.com\/authors\/.*/),
        publishedTime: mainWrapper.queryAttributeDifferences(0, 'time', 'datetime'),
        content: mainWrapper.queryAttr(0, 'class', 'c-entry-content '),
        sourceFile: htmlNames[0]
    });
    items.push({
        title: mainWrapper.queryTag(1, 'h1'),
        subtitle: mainWrapper.queryTag(1, 'h2'),
        author: mainWrapper.queryAttr(1, 'href', /.*www.theverge.com\/authors\/.*/),
        publishedTime: mainWrapper.queryAttributeDifferences(1, 'time', 'datetime'),
        content: mainWrapper.queryAttr(1, 'class', 'c-entry-content '),
        sourceFile: htmlNames[1]
    });
} else if(folder == 'overstock.com') {
    let wrapper1 = new Wrapper();
    let iterator1 = new DOMIterator(mainWrapper.simillarTags[3].tag1.subElements[mainWrapper.simillarTags[3].tag1.subElements.length - 1]);
    //first file
    for(let i = 0; i<mainWrapper.simillarTags[3].tag1.subElements.length; i++) {
        iterator1.reset();
        let iterator2 = new DOMIterator(mainWrapper.simillarTags[3].tag1.subElements[i]);
        p(iterator1, iterator2, wrapper1);
    }
    let namesAndPrices2 = wrapper1.queryTagArray(1, 'b');
    let listPrices2 = wrapper1.queryTagArray(1, 's');
    let savings2 = wrapper1.queryAttrArray(1, 'class', 'littleorange');
    let content2 = wrapper1.queryAttrArray(1, 'class', 'normal');
    let purchaseUrl2 = wrapper1.queryAttributeDifferencesArray(1, 'a', 'href');
    purchaseUrl2 = purchaseUrl2.filter(function(item, pos, self) {
        return self.indexOf(item) == pos;
    });
    for(let i = 0; i<namesAndPrices2.length; i+=2) {
        let savingSplit = savings2[i/2].split(' ');
        items.push({
            title: namesAndPrices2[i],
            listPrice: listPrices2[i/2],
            price: namesAndPrices2[i+1],
            saving: savingSplit[0],
            savingPercent: savingSplit[1].substring(1, savingSplit[1].length-1),
            content: content2[i/2],
            purchaseUrl: purchaseUrl2[i/2],
            sourceFile: htmlNames[0]
        });
    }
    let namesAndPrices1 = wrapper1.queryTagArray(0, 'b');
    let listPrices1 = wrapper1.queryTagArray(0, 's');
    let savings1 = wrapper1.queryAttr(0, 'class', 'littleorange');
    let savings1Split = savings1.split(' ');
    let content1 = wrapper1.queryAttr(0, 'class', 'normal');
    let purchaseUrl1 = wrapper1.queryAttributeDifferences(0, 'a', 'href');
    items.push({
        title: namesAndPrices1[0],
        listPrice: listPrices1[0],
        price: namesAndPrices1[1],
        saving: savings1Split[0],
        savingPercent: savings1Split[1].substring(1, savings1Split[1].length-1),
        content: content1,
        purchaseUrl: purchaseUrl1,
        sourceFile: htmlNames[0]
    });

    //second file
    let wrapper2 = new Wrapper();
    for(let i = 0; i<mainWrapper.simillarTags[3].tag2.subElements.length; i++) {
        iterator1.reset();
        let iterator2 = new DOMIterator(mainWrapper.simillarTags[3].tag2.subElements[i]);
        p(iterator1, iterator2, wrapper2);
    }
    let namesAndPrices3 = wrapper2.queryTagArray(1, 'b');
    let listPrices3 = wrapper2.queryTagArray(1, 's');
    let savings3 = wrapper2.queryAttrArray(1, 'class', 'littleorange');
    let content3 = wrapper2.queryAttrArray(1, 'class', 'normal');
    let purchaseUrl3 = wrapper2.queryAttributeDifferencesArray(1, 'a', 'href');
    purchaseUrl3 = purchaseUrl3.filter(function(item, pos, self) {
        return self.indexOf(item) == pos;
    });
    for(let i = 0; i<namesAndPrices3.length; i+=2) {
        let savingSplit = savings3[i/2].split(' ');
        items.push({
            title: namesAndPrices3[i],
            listPrice: listPrices3[i/2],
            price: namesAndPrices3[i+1],
            saving: savingSplit[0],
            savingPercent: savingSplit[1].substring(1, savingSplit[1].length-1),
            content: content3[i/2],
            purchaseUrl: purchaseUrl3[i/2],
            sourceFile: htmlNames[1]
        });
    }
}

if(items.length > 0) console.log(JSON.stringify(items, null, 2));