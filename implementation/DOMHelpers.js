const cleaner = require('clean-html');

class Tag {
  constructor(name, parent) {
    this.name = name;
    this.parentTag = parent;
    this.attributes = [];
    this.subElements = [];
    this.content = '';
  }

  newChild(child) {
    this.subElements.push(child);
  }

  appendContent(content) {
    this.content += content.trim();
  }

  addAttribute(attribute) {
    this.attributes.push(attribute);
  }
}

class Attribute {
  constructor(key, value) {
    this.key = key;
    this.value = value;
  }
}

class DOMBuilder {
  constructor(html) {
    let cleaned = false;
    cleaner.clean(html, {
      'wrap': 0,
      'remove-comments': true,
      'remove-tags': ['script', 'meta', 'link', 'br', 'p', 'strong', 'noscript', 'svg', 'figure'],
      'add-break-around-tags': ['nav', 'li', 'ul', 'span', 'svg', 'nav', 'use']
    }, result => {
      cleaned = true;
      this.html = result;
    });
    while(!cleaned) {} //wait for callback :D
    this.lines = this.html.split('\n');
    this.rootTag = new Tag('root', null);
    let currentTag = this.rootTag;
    for (let i = 0; i < this.lines.length; i++) {
      currentTag = this.processLine(this.lines[i], currentTag);
    }
    this.clean();
  }

  processLine(line, originalCurrentTag) {
    let split = line.trim().split('');
    let tagOpened = false;
    let findingName = false;
    let findingAttributes = false;
    let findingAttributeName = false;
    let openedBrackets = false;
    let closingTag = false;
    let instaClose = false;
    let newName = '';
    let currentContent = '';
    let currentAtrributeName = '';
    let currentAttributeValue = '';
    let currentTag = originalCurrentTag;
    for (let i = 0; i < split.length; i++) {
      if (split[i] == '<') {
        tagOpened = true;
        findingName = true;
        if (currentContent != '') {
          currentTag.appendContent(currentContent);
          currentContent = '';
        }
      } else if (split[i] == '>') {
        tagOpened = false;
        findingAttributes = false;
        if (closingTag || instaClose) {
          instaClose = false;
          closingTag = false;
          findingName = false;
          newName = '';
          currentTag = currentTag.parentTag;
        } else {
          if (findingName) {
            if (newName == 'img' || newName == '!DOCTYPE' || newName == 'input' || newName == 'br' || newName == 'circle' || newName == 'path' || newName == 'use' || newName == 'source') {
              instaClose = true;
            }
            findingName = false;
            let newTag = new Tag(newName, currentTag);
            currentTag.newChild(newTag);
            newName = '';
            currentTag = newTag;
          }
        }
      } else if (tagOpened && findingName && split[i] == '/') {
        closingTag = true;
      } else if (tagOpened && findingName && split[i] != ' ') {
        newName += split[i];
      } else if (tagOpened && findingName && split[i] == ' ') {
        if (newName == 'img' || newName == '!DOCTYPE' || newName == 'input' || newName == 'br' || newName == 'circle' || newName == 'path' || newName == 'use' || newName == 'source') {
          instaClose = true;
        }
        findingName = false;
        let newTag = new Tag(newName, currentTag);
        currentTag.newChild(newTag);
        currentTag = newTag;
        newName = '';
        findingAttributes = true;
        findingAttributeName = true;
      } else if (!tagOpened) {
        currentContent += split[i];
      } else if (tagOpened && findingAttributes && findingAttributeName && split[i] == '=') {
        findingAttributeName = false;
      } else if (tagOpened && findingAttributes && (split[i] == '\'' || split[i] == '"')) {
        if (!openedBrackets) {
          openedBrackets = true;
        } else {
          openedBrackets = false;
          currentTag.addAttribute(new Attribute(currentAtrributeName, currentAttributeValue))
          currentAtrributeName = '';
          currentAttributeValue = '';
          findingAttributeName = true;
        }
      } else if (tagOpened && findingAttributes && split[i] == '/' && !openedBrackets) {
        findingAttributes = false;
        instaClose = true;
      } else if (tagOpened && findingAttributes && split[i] != ' ') {
        if (findingAttributeName) {
          currentAtrributeName += split[i];
        } else {
          currentAttributeValue += split[i];
        }
      } else if (tagOpened && findingAttributes && split[i] == ' ') {
        if (openedBrackets) {
          //space is allowed
          currentAttributeValue += ' ';
        } else {
          //space is not allowed
        }
      } else if (!tagOpened) {
        currentContent += split[i];
      }
    }
    if (currentContent != '') {
      currentTag.appendContent(currentContent + '\n');
    }
    return currentTag;
  }

  clean() {
    this.cleanTag(this.rootTag);
  }

  //remove everything with empty content, all br elements
  cleanTag(tag) {
    let numberOfSubItems = tag.subElements.length;
    if (numberOfSubItems == 0) {
      if (tag.content == '') {
        return true;
      } else if (tag.name == 'br') {
        return true;
      } else {
        return false;
      }
    } else {
      let cleaned = 0;
      let toRemove = [];
      for (let i = 0; i < tag.subElements.length; i++) {
        if (this.cleanTag(tag.subElements[i])) {
          cleaned++;
          toRemove.unshift(i);
        }
      }
      toRemove.forEach(item => {
        tag.subElements.splice(item, 1);
      });
      if (numberOfSubItems == cleaned) {
        return true;
      } else {
        return false;
      }
    }
  }

  queryAttr(attr, value) {
    return this.queryAttr2(this.rootTag, attr, value);
  }

  queryAttr2(current, attr, value) {
    for(let i = 0; i<current.attributes.length; i++) {
      let a = current.attributes[i];
      if(a.key == attr && a.value == value) {
        return current.content;
      }
    }
    for(let i = 0; i<current.subElements.length; i++) {
      let returned = this.queryAttr2(current.subElements[i], attr, value);
      if(returned) {
        return returned;
      }
    }
  }
}

class DOMIterator {
  constructor(tag) {
    this.current = tag;
    this.currentSubItem = 0;
    this.saved = null;
  }

  next() {
    if (this.current.subElements.length > this.currentSubItem) {
      this.currentSubItem++;
      return this;
    }
  }

  previous() {
    if (this.currentSubItem > 0) {
      this.currentSubItem--;
      return this;
    }
  }

  value() {
    return this.current.subElements[this.currentSubItem];
  }

  goDeeper() {
    return new DOMIterator(this.current.subElements[this.currentSubItem]);
  }

  save() {
    this.saved = this.currentSubItem;
  }

  restore() {
    this.currentSubItem = this.saved;
  }

  reset() {
    this.currentSubItem = 0;
  }
}

class ContentDifference {
  constructor(tag1, tag2) {
    this.tagName = tag1.name;
    this.attributes = tag1.attributes;
    this.value1 = tag1.content;
    this.value2 = tag2.content;
    this.parent = tag1.parentTag;
  }
}

class AttributeDifference {
  constructor(tag, key1, value1, key2, value2) {
    this.tagName = tag.name;
    this.key1 = key1;
    this.key2 = key2;
    this.value1 = value1;
    this.value2 = value2;
  }
}

class Similarity {
  constructor(tag1, tag2) {
    this.tag1 = tag1;
    this.tag2 = tag2;
  }
}

class Wrapper {
  constructor() {
    this.contentDifferences = [];
    this.attributeDifferences = [];
    this.simillarTags = [];
  }

  queryAttr(index, attr, value) {
    for(let ix = 0; ix<this.contentDifferences.length; ix++) {
      let cd = this.contentDifferences[ix];
      let current = cd;
      while (current != null) {
        for(let i = 0; i<current.attributes.length; i++) {
          let a = current.attributes[i];
          if(value instanceof RegExp) {
            if(a.key == attr && value.test(a.value)) {
              if (index == 0) {
                return cd.value1;
              } else {
                return cd.value2;
              }
            }
          } else {
            if (a.key == attr && a.value == value) {
              if (index == 0) {
                return cd.value1;
              } else {
                return cd.value2;
              }
            }
          }
        }
        current = current.parentTag;
      }
    }
    return null;
  }

  queryAttrArray(index, attr, value) {
    let results = [];
    for(let ix = 0; ix<this.contentDifferences.length; ix++) {
      let cd = this.contentDifferences[ix];
      let current = cd;
      while (current != null) {
        for(let i = 0; i<current.attributes.length; i++) {
          let a = current.attributes[i];
          if(value instanceof RegExp) {
            if(a.key == attr && value.test(a.value)) {
              if (index == 0) {
                results.push(cd.value1);
              } else {
                results.push(cd.value2);
              }
            }
          } else {
            if (a.key == attr && a.value == value) {
              if (index == 0) {
                results.push(cd.value1);
              } else {
                results.push(cd.value2);
              }
            }
          }
        }
        current = current.parentTag;
      }
    }
    return results;
  }

  queryAttributeDifferences(index, tag, key) {
    for(let i = 0; i<this.attributeDifferences.length; i++) {
      let attribute = this.attributeDifferences[i];
      if(attribute.tagName == tag && attribute.key1 == key) {
        if(index == 0) {
          return attribute.value1;
        } else {
          return attribute.value2;
        }
      }
    }
  }

  queryAttributeDifferencesArray(index, tag, key) {
    let results = [];
    for(let i = 0; i<this.attributeDifferences.length; i++) {
      let attribute = this.attributeDifferences[i];
      if(attribute.tagName == tag && attribute.key1 == key) {
        if(index == 0) {
          results.push(attribute.value1);
        } else {
          results.push(attribute.value2);
        }
      }
    }
    return results;
  }

  queryTag(index, tag) {
    for(let ix = 0; ix<this.contentDifferences.length; ix++) {
      let cd = this.contentDifferences[ix];
      let currentLevel = 0;
      let current = cd;
      while (currentLevel < 100 && current != null) {
        if(current.tagName == tag) {
          if(index == 0) {
            return current.value1;
          } else {
            return current.value2;
          }
        }
        current = current.parentTag;
      }
    }
    return null;
  }

  queryTagArray(index, tag) {
    let results = [];
    for(let ix = 0; ix<this.contentDifferences.length; ix++) {
      let cd = this.contentDifferences[ix];
      let currentLevel = 0;
      let current = cd;
      while (currentLevel < 100 && current != null) {
        if(current.tagName == tag) {
          if(index == 0) {
            results.push(current.value1);
          } else {
            results.push(current.value2);
          }
        }
        current = current.parentTag;
      }
    }
    return results;
  }

  addAttributeDifference(attrdfr) {
    this.attributeDifferences.push(attrdfr);
  }

  addContentDifference(cntdfr) {
    this.contentDifferences.push(cntdfr);
  }

  addSimilarity(simil) {
    this.simillarTags.push(simil);
  }
}

function equal(tag1, tag2) {
  if (tag1.subElements.length != tag2.subElements.length) {
    return false;
  }
  if (tag1.attributes.length != tag2.attributes.length) {
    return false;
  }
  if (tag1.content != tag2.content) {
    return false;
  }
  for (let i = 0; i < tag1.attributes.length; i++) {
    if (tag1.attributes[i].key == tag2.attributes[i].key && tag1.attributes[i].value == tag2.attributes[i].value) {

    } else {
      return false;
    }
  }
  for (let i = 0; i < tag1.subElements.length; i++) {
    if (!equal(tag1.subElements[i], tag2.subElements[i])) return false;
  }
  return true;
}

function closelyEqual(tag1, tag2) {
  if(tag1 == null || tag2 == null) {
    return false;
  }
  if (tag1.attributes.length != tag2.attributes.length) {
    return false;
  } else {
    for (let i = 0; i < tag1.attributes.length; i++) {
      if (tag1.attributes[i].key != tag2.attributes[i].key) {
        return false;
      }
    }
    if(tag1.name == tag2.name) {
      return true;
    }
  }
  return false;
}

function findDifferences(tag1, tag2, wrapper) {
  for (let i = 0; i < tag1.attributes.length; i++) {
    if (tag1.attributes[i].key != tag2.attributes[i].key || tag1.attributes[i].value != tag2.attributes[i].value) {
      wrapper.addAttributeDifference(new AttributeDifference(tag1, tag1.attributes[i].key, tag1.attributes[i].value, tag2.attributes[i].key, tag2.attributes[i].value));
    }
  }
  if (tag1.content != tag2.content) {
    wrapper.addContentDifference(new ContentDifference(tag1, tag2));
  }

  //check if all items are the closelyTheSame, but not the same number of them
  if(tag1.subElements.length != tag2.subElements.length) {
    let closelyTheSame = true;
    for (let i = 1; i < tag1.subElements.length; i++) {
      if(!closelyEqual(tag1.subElements[0], tag1.subElements[i])) {
        closelyTheSame = false;
      }
    }
    for (let i = 1; i < tag2.subElements.length; i++) {
      if(!closelyEqual(tag2.subElements[0], tag2.subElements[i])) {
        closelyTheSame = false;
      }
    }
    if(closelyTheSame) {
      wrapper.addSimilarity(new Similarity(tag1, tag2));
    }
  }
  

  let firstOffset = 0;
  let secondOffset = 0;
  for (let i = 0; i < tag1.subElements.length; i++) {
    if(closelyEqual(tag1.subElements[i + firstOffset], tag2.subElements[i + secondOffset])) {
      findDifferences(tag1.subElements[i + firstOffset], tag2.subElements[i + secondOffset], wrapper);
    } else {
      for(let j = i; j<tag1.subElements.length; j++) {
        if(closelyEqual(tag1.subElements[j + firstOffset], tag2.subElements[i + secondOffset])) {
          firstOffset += j-i;
          i--;
          break;
        }
        if(closelyEqual(tag1.subElements[i + firstOffset], tag2.subElements[j + secondOffset])) {
          secondOffset += j-i;
          i--;
          break;
        }
      }
      
    }
  }
}

const TagUtils = {
  equal,
  closelyEqual,
  findDifferences
};

module.exports = {
  DOMBuilder,
  DOMIterator,
  Wrapper,
  TagUtils
};