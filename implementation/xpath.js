const fs = require('fs');
const xpath = require('xpath');
const dom = require('xmldom').DOMParser;
const htmlToText = require('html-to-text');
const sanitizeHtml = require('sanitize-html');

let validFolders = ['overstock.com', 'rtvslo.si', 'theverge.com'];
let folder = process.argv[2];
if(!validFolders.includes(folder)) {
    console.log('Invalid folder name provided.');
    process.exit(1);
}

let data;
try {
    data = fs.readdirSync(`../input//${folder}`);
} catch (e) {
    console.log('Something went wrong opening the folder. Check if the selected folder exists.');
    process.exit(1);
}

let items = [];
data.forEach(file => {
    let found = file.match(/.html/);
    if(found) {
        let htmlFile = fs.readFileSync(`../input//${folder}/${file}`).toString();
        let d = new dom({
            errorHandler: { warning: () => {}}
        }).parseFromString(htmlFile);
        if(folder === 'overstock.com') {  
            let titles = xpath.select('//table//table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/a/b', d);
            let listPrices = xpath.select("//table//table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td[2]/s", d);
            let prices = xpath.select("//table//table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]/span/b", d);
            let savings = xpath.select("//table//table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[3]/td[2]/span", d);
            let contents = xpath.select("//table//table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/span", d);
            let purchaseUrls = xpath.select("//table//table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/span/a/@href", d);
            for(let i = 0; i<titles.length; i++) {
                let item = {};
                item.title = titles[i].firstChild.nodeValue;
                item.listPrice = listPrices[i].firstChild.nodeValue;
                item.price = prices[i].firstChild.nodeValue;
                let saving = savings[i].firstChild.nodeValue.split(" ");
                item.saving = saving[0];
                item.savingPercent = saving[1].slice(1, -1);
                item.content = contents[i].firstChild.nodeValue.replace(/\n/g, ' ');
                item.purchaseUrl = purchaseUrls[i].nodeValue;
                item.sourceFile = file;
                items.push(item);
            }
        } else if(folder === 'rtvslo.si') {
            let item = {};
            item.title = xpath.select('//header/h1', d)[0].firstChild.nodeValue;
            item.subtitle = xpath.select('//header/div[@class="subtitle"]', d)[0].firstChild.nodeValue;
            item.author = xpath.select('//div[@class="author-name"]', d)[0].firstChild.nodeValue;
            let publish = xpath.select('//div[@class="publish-meta"]', d)[0].firstChild.nodeValue.replace(/\n/g, ' ').replace(/\t/g, '  ').trim().split('ob');
            item.publishedTime = {
                date: publish[0].trim(),
                hour: publish[1].trim()
            };
            item.lead = xpath.select('//p[@class="lead"]', d)[0].firstChild.nodeValue;
            item.content = htmlToText.fromString(sanitizeHtml(xpath.select("//article", d)[0].toString(), {
                nonTextTags: [ 'style', 'script', 'textarea', 'noscript', 'figure' ]
            }), {
                ignoreImage: true,
                ignoreHref: true,
                preserveNewlines: false
            }).replace(/\r?\n|\r/g, ' ').replace(/\t/g, ' ').replace('  ', ' ').trim();
            item.sourceFile = file;
            items.push(item);
        } else if(folder == 'theverge.com') {
            let item = {};
            item.title = xpath.select('/html/body/div[1]/section/section/div[1]/div[1]/div[2]/h1', d)[0].firstChild.nodeValue;
            item.subtitle = xpath.select('/html/body/div[1]/section/section/div[1]/div[1]/h2', d)[0].firstChild.nodeValue;
            item.author = xpath.select('/html/body/div[1]/section/section/div[1]/div[1]/div[3]/span[1]/a', d)[0].firstChild.nodeValue;
            item.publishedTime = xpath.select('/html/body/div[1]/section/section/div[1]/div[1]/div[3]/span[2]/time', d)[0].firstChild.nodeValue.replace(/\r?\n|\r/g, ' ').trim();
            item.content = htmlToText.fromString(sanitizeHtml(xpath.select('/html/body/div/section/section/div[2]/div[1]/div[1]', d)[0].toString(), {
                nonTextTags: [ 'style', 'script', 'textarea', 'noscript', 'figure' ]
            }), {
                ignoreImage: true,
                ignoreHref: true,
                preserveNewlines: false
            }).replace(/\r?\n|\r/g, ' ').replace(/\t/g, ' ').replace('  ', ' ').trim();
            item.sourceFile = file;
            items.push(item);
        }
    }
});
console.log(JSON.stringify(items, null, 2));