const fs = require('fs');
const htmlToText = require('html-to-text');
const sanitizeHtml = require('sanitize-html');

let validFolders = ['overstock.com', 'rtvslo.si', 'theverge.com'];
let folder = process.argv[2];
if(!validFolders.includes(folder)) {
    console.log('Invalid folder name provided.');
    process.exit(1);
}

let data;
try {
    data = fs.readdirSync(`../input/${folder}`);
} catch (e) {
    console.log('Something went wrong opening the folder. Check if the selected folder exists.');
    process.exit(1);
}
let items = [];
data.forEach(file => {
    let found = file.match(/.html/);
    if(found) {
        let htmlFile = fs.readFileSync(`../input//${folder}/${file}`).toString();
        let match; 
        if(folder === 'overstock.com') {      
            let regex = /<td valign="top">\s*<a href=".*?"><b>(.*?)<\/b>.*?List Price:.*?<s>(.*?)<\/s>.*?Price:.*?<b>(.*?)<\/b>.*?>You Save:.*?class=\"littleorange\">(.*?)\((.*?)\).*?class=\"normal\">(.*?)<br><a href=\"(.*?)\">/gs;
            while(match = regex.exec(htmlFile)) {
                items.push({
                    title: match[1],
                    listPrice: match[2],
                    price: match[3],
                    saving: match[4].trim(),
                    savingPercent: match[5],
                    content: match[6].replace(/\n/g, ' '),
                    purchaseUrl: match[7],
                    sourceFile: file
                });
            }
        } else if(folder === 'rtvslo.si') {
            let regex = /<h1>(.*)<\/h1>.*subtitle\">(.*?)<\/div>.*?strong>(.*?)<\/strong>\|\s(.*?)\sob\s(.*?)\t*\n.*?class=\"lead\">(.*?)<\/p>.*?class=\"article-body">(.*)<\/article>\s*<\/div>/gs;
            while(match = regex.exec(htmlFile)) {
                items.push({
                    title: match[1],
                    subtitle: match[2],
                    author: match[3],
                    publishedTime: {
                        date: match[4],
                        hour: match[5]
                    },
                    lead: match[6],
                    content: htmlToText.fromString(sanitizeHtml(match[7] + '</article>', {
                        nonTextTags: [ 'style', 'script', 'textarea', 'noscript', 'figure' ]
                    }), {
                        ignoreImage: true,
                        ignoreHref: true,
                        preserveNewlines: false
                    }).replace(/\r?\n|\r/g, ' ').replace(/\t/g, ' ').replace('  ', ' ').trim(),
                    sourceFile: file
                });
            }
        } else if(folder === 'theverge.com') {
            let regex = /<[^>]* class="c-page-title">(.+?)<\/[^>]*>.*<[^>]* class="c-entry-summary p-dek">(.+?)<\/[^>].*<a href="https:\/\/www\.theverge\.com\/authors\/.*?>(.*?)<\/a>.*?datetime="(.*?)".*<[^>]* class="c-entry-content ">(.+?)<div class="u-hidden-text"/gsm;
            while(match = regex.exec(htmlFile)) {
                items.push({
                    title: match[1] || '',
                    subtitle: match[2] || '',
                    author: match[3] || '',
                    publishedTime:  match[4] || '',
                    content: htmlToText.fromString(sanitizeHtml(match[5], {
                        nonTextTags: [ 'style', 'script', 'textarea', 'noscript', 'figure' ]
                    }), {
                        ignoreImage: true,
                        ignoreHref: true,
                        preserveNewlines: false
                    }).replace(/\r?\n|\r/g, ' ').replace(/\t/g, ' ').replace('  ', ' ').trim(),
                    sourceFile: file
                });
            }

        }
    }
});
console.log(JSON.stringify(items, null, 2));
