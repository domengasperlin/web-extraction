
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"113",
  "macros":[{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return\"embed\"===document.querySelector(\"body\").getAttribute(\"data-analytics-class\")})();"]
    },{
      "function":"__aev",
      "vtp_varType":"ELEMENT"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"data-analytics-link\")||void 0})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"data-analytics-class\")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",1],8,16],";if(a=a.parentElement)return a.getAttribute(\"data-analytics-class\")||a.getAttribute(\"data-analytics-action\")||void 0})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"data-analytics-social\")||void 0})();"]
    },{
      "function":"__v",
      "vtp_setDefaultValue":false,
      "vtp_name":"GA CrossDomains",
      "vtp_dataLayerVersion":2
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var b=",["escape",["macro",1],8,16],".hostname||\"\";if(!b||b==document.domain)return!1;for(var c=(",["escape",["macro",6],8,16],"||\"\").split(\",\"),a=0;a\u003Cc.length;a++)if(c[a]\u0026\u00260\u003C=b.indexOf(c[a]))return!1;return!0})();"]
    },{
      "function":"__v",
      "vtp_name":"Breakpoint",
      "vtp_dataLayerVersion":2
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",8],8,16],"||\"\";if(\"string\"===typeof a\u0026\u0026a.match(\/small\/)\u0026\u0026!a.match(\/medium\/))return!0;a=document.documentElement;return 600\u003E=(window.innerWidth||a\u0026\u0026a.clientWidth||document.body.clientWidth)})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return document.title.replace(\/\\s*-\\s+TODO-UnisonDomain\\.com$\/,\"\")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return function(){if(window.history.replaceState){var a=window.location.search.replace(\/(_ga|utm_)[^\u0026]+\u0026?\/g,\"\").replace(\/\u0026$\/,\"\").replace(\/^\\?$\/,\"\");window.history.replaceState({},document.title,window.location.pathname+a+window.location.hash)}return null}})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a=",["escape",["macro",1],8,16],";a\u0026\u0026a.getAttribute;){var b=a.getAttribute(\"data-analytics-placement\");if(b)return\":\"+b;a=a.parentNode}return\"\"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a=",["escape",["macro",1],8,16],";a\u0026\u0026a.getAttribute;){var b=a.getAttribute(\"data-analytics-action\");if(b)return b;a=a.parentNode}return\"share\"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a=",["escape",["macro",1],8,16],";a\u0026\u0026a.getAttribute;){var b=a.getAttribute(\"data-analytics-placement\");if(b)return b;a=a.parentNode}return\"main\"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var b=",["escape",["macro",1],8,16],",a=b.querySelectorAll(\"h1,h2,h3,h4,h5,h6\");return a.length?(a=Array.prototype.slice.call(a),a.sort(function(a,b){return a.tagName.localeCompare(b.tagName)}),a[0].innerText):b.innerText})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",1],8,16],",b=a.getAttribute(\"data-analytics-link\"),c=b.replace(\/([^:]+).*\/,\"$1\"),d=function(a){var b=a.querySelectorAll(\"h1,h2,h3,h4,h5,h6\");return b.length?(b=[].slice.call(b).sort(function(a,b){return a.tagName.localeCompare(b.tagName)}),b[0].innerText):a.innerText};if(b!==c){for(b='[data-analytics-link\\x3d\"'+c+'\"]';(a=a.parentNode)\u0026\u0026a.querySelector;)if(c=a.querySelector(b))return d(c);return ",["escape",["macro",1],8,16],".getAttribute(\"href\")}return d(a)})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"data-analytics-viewport\")||void 0})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var b=!0!==window.adblockJsFile;if(void 0!==navigator.doNotTrack)var a=navigator.doNotTrack;else void 0!==window.doNotTrack?a=window.doNotTrack:void 0!==navigator.msDoNotTrack\u0026\u0026(a=navigator.msDoNotTrack);a=void 0!==a?\/1|yes|true\/.test(String(a).toLowerCase())?\"true\":\"false\":\"undefined\";return\"adblock-\"+b+\":dnt-\"+a})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return Array.isArray(window.chorusSDK)})();"]
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"data-analytics-label\")||",["escape",["macro",20],8,16],"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"data-analytics-campaign\")||void 0})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",1],8,16],".getAttribute(\"value\")})();"]
    },{
      "function":"__e"
    },{
      "function":"__aev",
      "vtp_varType":"URL"
    },{
      "function":"__c",
      "vtp_value":"auto"
    },{
      "function":"__v",
      "vtp_name":"Content ID",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Content Type",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Author",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"All Chorus Categories",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Publish Date",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Entry Groups",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Community",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Network",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"GA Primary ID"
    },{
      "function":"__v",
      "vtp_name":"socialNetwork",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"socialAction",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"socialTarget",
      "vtp_dataLayerVersion":2
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_name":"Hour of Publish",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Advertiser",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Campaign",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Pagination Tracking",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Program",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Topic",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Super Groups",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Additional Placements",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Demand Post",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Hour of Day",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Day of Week",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Hour of Update",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Logged in Status",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Last Time Updated",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Page Layout Version"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Hermano Records"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"iFrame Tracking"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Hidden Groups"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Story Word Count"
    },{
      "function":"__j",
      "vtp_name":"navigator.userAgent"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ScrollSubscription"
    },{
      "function":"__c",
      "vtp_value":"Non AMP"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Piano Logged In Status"
    },{
      "function":"__v",
      "vtp_name":"virtualPagePath",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Embed Host",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"virtualPageTitle",
      "vtp_dataLayerVersion":2
    },{
      "function":"__aev",
      "vtp_varType":"CLASSES"
    },{
      "function":"__v",
      "vtp_name":"eventNonInt",
      "vtp_dataLayerVersion":2
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return ",["escape",["macro",9],8,16],"?\"mobile\":\"desktop\"})();"]
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"GA Secondary ID"
    },{
      "function":"__c",
      "vtp_value":"unison"
    },{
      "function":"__v",
      "vtp_name":"gtm.triggers",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":""
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_stripWww":true,
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_name":"eventCategory",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventAction",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventLabel",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventValue",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Video ID"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Video Content Type"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Producer"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Video Publish Date"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Tags"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Primary Brand"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Series"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Format"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Sub-format"
    },{
      "function":"__c",
      "vtp_value":"2724"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"chartbeat_domain"
    },{
      "function":"__v",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2,
      "vtp_defaultValue":"",
      "vtp_name":"chartbeat_authors"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"chartbeat_zone"
    },{
      "function":"__v",
      "vtp_name":"errorType",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"placement",
      "vtp_dataLayerVersion":2
    },{
      "function":"__aev",
      "vtp_varType":"ID"
    },{
      "function":"__aev",
      "vtp_varType":"TARGET"
    },{
      "function":"__e"
    },{
      "function":"__f"
    },{
      "function":"__f",
      "vtp_component":"HOST",
      "vtp_stripWww":false
    },{
      "function":"__f",
      "vtp_component":"PATH"
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"lazyLoadedAdsEnabled"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":true,
      "vtp_varType":"CLASSES",
      "vtp_defaultValue":"more"
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    }],
  "tags":[{
      "function":"__ua",
      "priority":5,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"email",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["template",["macro",28],":newsletter",["macro",12]],
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":81
    },{
      "function":"__ua",
      "priority":5,
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"comment",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["template",["macro",28],":comment:",["macro",23]],
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]],["map","index","2","dimension",["macro",28]],["map","index","3","dimension",["macro",52]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":111
    },{
      "function":"__ua",
      "priority":3,
      "vtp_trackingId":["macro",35],
      "vtp_trackType":"TRACK_SOCIAL",
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_useDebugVersion":false,
      "vtp_enableLinkId":true,
      "vtp_doubleClick":true,
      "vtp_advertisingFeaturesType":"DISPLAY_FEATURES",
      "vtp_socialNetwork":["macro",36],
      "vtp_socialAction":["macro",37],
      "vtp_socialActionTarget":["macro",38],
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsSocial":true,
      "tag_id":73
    },{
      "function":"__ua",
      "priority":3,
      "vtp_trackingId":["macro",35],
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","27","dimension",["macro",43]],["map","index","2","dimension",["macro",28]],["map","index","33","dimension",["macro",64]]],
      "vtp_useDebugVersion":false,
      "vtp_autoLinkDomains":["macro",6],
      "vtp_enableLinkId":true,
      "vtp_decorateFormsAutoLink":true,
      "vtp_doubleClick":true,
      "vtp_advertisingFeaturesType":"DISPLAY_FEATURES",
      "vtp_useHashAutoLink":false,
      "vtp_enableEcommerce":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","page","value",["macro",63]],["map","fieldName","title","value",["macro",65]]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":78
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"social",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["template",["macro",13],":",["macro",28],":",["macro",68],":",["macro",14]],
      "vtp_eventLabel":["template",["macro",13],":",["macro",5],":",["macro",28],":",["macro",68],":",["macro",14]],
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":84
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"social",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["template",["macro",68],":more"],
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":104
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"rss",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["template","click:rss:",["macro",72]],
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":106
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",28],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"feature:bottom:promotion",
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":121
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"poll",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"click:poll:vote",
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":123
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"poll",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"click:poll:return",
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","2","dimension",["macro",28]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":124
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"poll",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"click:poll:view",
      "vtp_eventLabel":["macro",39],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","2","dimension",["macro",28]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":125
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",28],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"click:citybox",
      "vtp_eventLabel":["macro",72],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":148
    },{
      "function":"__ua",
      "priority":3,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",74],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",75],
      "vtp_eventLabel":["macro",76],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_eventValue":["macro",77],
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","2","dimension",["macro",28]],["map","index","27","dimension",["macro",43]],["map","index","33","dimension",["macro",64]],["map","index","20","dimension",["macro",30]],["map","index","4","dimension",["macro",29]],["map","index","5","dimension",["macro",31]],["map","index","10","dimension",["macro",32]],["map","index","11","dimension",["macro",33]],["map","index","12","dimension",["macro",34]],["map","index","16","dimension",["macro",44]],["map","index","17","dimension",["macro",45]],["map","index","41","dimension",["macro",78]],["map","index","42","dimension",["macro",79]],["map","index","43","dimension",["macro",80]],["map","index","44","dimension",["macro",81]],["map","index","45","dimension",["macro",82]],["map","index","46","dimension",["macro",83]],["map","index","47","dimension",["macro",84]],["map","index","48","dimension",["macro",85]],["map","index","49","dimension",["macro",86]],["map","index","53","dimension",["macro",58]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":149
    },{
      "function":"__ua",
      "priority":2,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_eventCategory":["macro",28],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":true,
      "vtp_eventAction":["template","link:",["macro",2],["macro",12]],
      "vtp_eventLabel":["macro",16],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":85
    },{
      "function":"__ua",
      "priority":2,
      "once_per_event":true,
      "vtp_nonInteraction":["macro",67],
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_eventCategory":["macro",17],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":true,
      "vtp_eventAction":["template","click:",["macro",2],["macro",12]],
      "vtp_eventLabel":["macro",16],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":92
    },{
      "function":"__ua",
      "priority":1,
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_autoLinkDomains":["macro",6],
      "vtp_decorateFormsAutoLink":true,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","hitCallback","value",["macro",11]],["map","fieldName","campaignMedium","value","social"],["map","fieldName","campaignSource","value","facebook"],["map","fieldName","title","value",["macro",65]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","20","dimension",["macro",30]],["map","index","26","dimension",["macro",40]],["map","index","18","dimension",["macro",41]],["map","index","19","dimension",["macro",42]],["map","index","27","dimension",["macro",43]],["map","index","16","dimension",["macro",44]],["map","index","17","dimension",["macro",45]],["map","index","12","dimension",["macro",34]],["map","index","13","dimension",["macro",46]],["map","index","14","dimension",["macro",47]],["map","index","15","dimension",["macro",48]],["map","index","2","dimension",["macro",28]],["map","index","22","dimension",["macro",49]],["map","index","21","dimension",["macro",50]],["map","index","11","dimension",["macro",33]],["map","index","9","dimension",["macro",51]],["map","index","3","dimension",["macro",52]],["map","index","4","dimension",["macro",29]],["map","index","6","dimension",["macro",18]],["map","index","5","dimension",["macro",31]],["map","index","8","dimension",["macro",53]],["map","index","1","dimension",["macro",27]],["map","index","10","dimension",["macro",32]],["map","index","7","dimension",["macro",8]],["map","index","29","dimension",["macro",70]],["map","index","34","dimension",["macro",55]],["map","index","25","dimension",["macro",56]],["map","index","53","dimension",["macro",58]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":100
    },{
      "function":"__ua",
      "priority":0,
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"outbound",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",25],
      "vtp_eventLabel":["macro",20],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",27]],["map","index","2","dimension",["macro",28]],["map","index","4","dimension",["macro",29]],["map","index","20","dimension",["macro",30]],["map","index","5","dimension",["macro",31]],["map","index","10","dimension",["macro",32]],["map","index","11","dimension",["macro",33]],["map","index","12","dimension",["macro",34]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":72
    },{
      "function":"__fsl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "tag_id":75
    },{
      "function":"__lcl",
      "vtp_waitForTags":true,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"100",
      "tag_id":76
    },{
      "function":"__ua",
      "priority":0,
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_autoLinkDomains":["macro",6],
      "vtp_decorateFormsAutoLink":true,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","hitCallback","value",["macro",11]],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","20","dimension",["macro",30]],["map","index","26","dimension",["macro",40]],["map","index","18","dimension",["macro",41]],["map","index","19","dimension",["macro",42]],["map","index","27","dimension",["macro",43]],["map","index","16","dimension",["macro",44]],["map","index","17","dimension",["macro",45]],["map","index","12","dimension",["macro",34]],["map","index","13","dimension",["macro",46]],["map","index","14","dimension",["macro",47]],["map","index","15","dimension",["macro",48]],["map","index","2","dimension",["macro",28]],["map","index","22","dimension",["macro",49]],["map","index","21","dimension",["macro",50]],["map","index","11","dimension",["macro",33]],["map","index","9","dimension",["macro",51]],["map","index","3","dimension",["macro",52]],["map","index","4","dimension",["macro",29]],["map","index","6","dimension",["macro",18]],["map","index","5","dimension",["macro",31]],["map","index","8","dimension",["macro",53]],["map","index","1","dimension",["macro",27]],["map","index","10","dimension",["macro",32]],["map","index","7","dimension",["macro",8]],["map","index","29","dimension",["macro",54]],["map","index","34","dimension",["macro",55]],["map","index","25","dimension",["macro",56]],["map","index","50","dimension",["macro",57]],["map","index","53","dimension",["macro",58]],["map","index","54","dimension",["macro",59]],["map","index","23","dimension",["macro",60]],["map","index","33","dimension",["macro",61]],["map","index","24","dimension",["macro",62]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":77
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",28],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",4],
      "vtp_eventLabel":["macro",25],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":83
    },{
      "function":"__ua",
      "priority":0,
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_autoLinkDomains":["macro",6],
      "vtp_decorateFormsAutoLink":true,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_enableLinkId":true,
      "vtp_dimension":["list",["map","index","20","dimension",["macro",30]],["map","index","26","dimension",["macro",40]],["map","index","18","dimension",["macro",41]],["map","index","19","dimension",["macro",42]],["map","index","27","dimension",["macro",43]],["map","index","16","dimension",["macro",44]],["map","index","17","dimension",["macro",45]],["map","index","12","dimension",["macro",34]],["map","index","13","dimension",["macro",46]],["map","index","14","dimension",["macro",47]],["map","index","15","dimension",["macro",48]],["map","index","2","dimension",["macro",28]],["map","index","22","dimension",["macro",49]],["map","index","21","dimension",["macro",50]],["map","index","11","dimension",["macro",33]],["map","index","9","dimension",["macro",51]],["map","index","3","dimension",["macro",52]],["map","index","4","dimension",["macro",29]],["map","index","6","dimension",["macro",18]],["map","index","5","dimension",["macro",31]],["map","index","8","dimension",["macro",53]],["map","index","1","dimension",["macro",27]],["map","index","10","dimension",["macro",32]],["map","index","7","dimension",["macro",8]],["map","index","34","dimension",["macro",55]],["map","index","25","dimension",["macro",56]],["map","index","53","dimension",["macro",58]],["map","index","54","dimension",["macro",59]],["map","index","23","dimension",["macro",60]],["map","index","33","dimension",["macro",61]],["map","index","24","dimension",["macro",62]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",69],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":89
    },{
      "function":"__ua",
      "priority":0,
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]]],
      "vtp_eventCategory":["macro",22],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":true,
      "vtp_eventAction":["template","click:",["macro",22]],
      "vtp_eventLabel":["macro",21],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",35],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":99
    },{
      "function":"__twitter_website_tag",
      "once_per_event":true,
      "vtp_event_type":"PageView",
      "vtp_twitter_pixel_id":"nz4et",
      "tag_id":108
    },{
      "function":"__opt",
      "setup_tags":["list",["tag",21,0]],
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","hitCallback","value",["macro",11]],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_useDebugVersion":false,
      "vtp_optimizeContainerId":"GTM-M6PXKL3",
      "vtp_trackingId":"UA-189494-73",
      "tag_id":109
    },{
      "function":"__opt",
      "setup_tags":["list",["tag",21,0]],
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","hitCallback","value",["macro",11]],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_useDebugVersion":false,
      "vtp_optimizeContainerId":"GTM-54FC4VZ",
      "vtp_trackingId":"UA-26533115-1",
      "tag_id":110
    },{
      "function":"__gcs",
      "setup_tags":["list",["tag",21,0]],
      "once_per_load":true,
      "vtp_siteId":"pryr5r37zzkmeutts55nszygji",
      "tag_id":118
    },{
      "function":"__opt",
      "setup_tags":["list",["tag",21,0]],
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","anonymizeIp","value","false"],["map","fieldName","cookieDomain","value",["macro",26]],["map","fieldName","hitCallback","value",["macro",11]],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_useDebugVersion":false,
      "vtp_optimizeContainerId":"GTM-NDNNL7Q",
      "vtp_trackingId":"UA-189494-74",
      "tag_id":137
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"1434782_127",
      "tag_id":150
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"1434782_130",
      "tag_id":151
    },{
      "function":"__cl",
      "tag_id":152
    },{
      "function":"__cl",
      "tag_id":153
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var a=window._fbq||(window._fbq=[]);if(!a.loaded){var b=document.createElement(\"script\");b.async=!0;b.src=\"https:\/\/connect.facebook.net\/en_US\/fbds.js\";var c=document.getElementsByTagName(\"script\")[0];c.parentNode.insertBefore(b,c);a.loaded=!0}a.push([\"addPixelId\",\"594981607301768\"])})();window._fbq=window._fbq||[];window._fbq.push([\"track\",\"PixelInitialized\",{}]);window._fbq.push([\"track\",\"6026192431231\",{value:\"1.00\",currency:\"USD\"}]);\u003C\/script\u003E\n\u003Cnoscript\u003E\u003Cimg height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?id=594981607301768\u0026amp;ev=PixelInitialized\"\u003E\u003C\/noscript\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":67
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript id=\"analytics-chartbeat\" type=\"text\/gtmscript\"\u003Evar _sf_async_config={};_sf_async_config.uid=",["escape",["macro",87],8,16],";_sf_async_config.domain=",["escape",["macro",88],8,16],";_sf_async_config.useCanonical=!0;_sf_async_config.authors=",["escape",["macro",89],8,16],";_sf_async_config.zone=",["escape",["macro",90],8,16],";(function(){window._sf_endpt=(new Date).getTime();var a=document.createElement(\"script\");a.setAttribute(\"language\",\"javascript\");a.setAttribute(\"type\",\"text\/javascript\");a.setAttribute(\"src\",\"https:\/\/static.chartbeat.com\/js\/chartbeat.js\");document.body.appendChild(a)})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":68
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar _comscore=_comscore||[];_comscore.push({c1:\"2\",c2:\"7976662\"});(function(){var a=document.createElement(\"script\"),b=document.getElementsByTagName(\"script\")[0];a.async=!0;a.src=\"https:\/\/sb.scorecardresearch.com\/beacon.js\";b.parentNode.insertBefore(a,b)})();\u003C\/script\u003E\n\u003Cnoscript\u003E\n  \u003Cimg src=\"https:\/\/sb.scorecardresearch.com\/p?c1=2\u0026amp;c2=7976662\u0026amp;cv=2.0\u0026amp;cj=1\"\u003E\n\u003C\/noscript\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":70
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript data-gtmsrc=\"https:\/\/cdn0.vox-cdn.com\/packs\/chorus_sdk-789f86740edd88f79bfa.js\" type=\"text\/gtmscript\"\u003E\u003C\/script\u003E\n\u003Cscript type=\"text\/gtmscript\"\u003E(function(b){var c=window[b]||[],d=window.Chorus_SDK||{},e=window[b]={push:function(a){\"function\"===typeof a\u0026\u0026a(d)}};c.forEach(function(a){e.push(a)})})(\"chorusSDK\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":98
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version=\"1.1\",a.queue=[],b=e.createElement(f),b.async=!0,b.src=\"\/\/static.ads-twitter.com\/uwt.js\",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,\"script\");twq(\"init\",\"nzjqi\");twq(\"track\",\"PageView\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":112
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version=\"1.1\",a.queue=[],b=e.createElement(f),b.async=!0,b.src=\"\/\/static.ads-twitter.com\/uwt.js\",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,\"script\");twq(\"init\",\"nzjql\");twq(\"track\",\"PageView\");\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":113
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version=\"1.1\",a.queue=[],b=e.createElement(f),b.async=!0,b.src=\"\/\/static.ads-twitter.com\/uwt.js\",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,\"script\");twq(\"init\",\"nzjqm\");twq(\"track\",\"PageView\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":114
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version=\"1.1\",a.queue=[],b=e.createElement(f),b.async=!0,b.src=\"\/\/static.ads-twitter.com\/uwt.js\",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,\"script\");twq(\"init\",\"nzjqo\");twq(\"track\",\"PageView\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":115
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-curbed\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":129
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-eater\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":130
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-vox\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":131
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-polygon\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":133
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-recode\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":134
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-sbNation\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":135
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-theRinger\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":136
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-theVerge\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":138
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E!function(){var a=window.SambaTV=window.SambaTV||[];if(!a.track){if(a.invoked)return void(window.console\u0026\u0026window.console.error\u0026\u0026window.console.error(\"Samba Metrics snippet included twice.\"));a.invoked=!0;a.methods=\"track Impression Purchase Register Click Login Register\".split(\" \");a.factory=function(e){return function(){var b=Array.prototype.slice.call(arguments);return b.unshift(e),a.push(b),a}};for(var c=0;c\u003Ca.methods.length;c++){var d=a.methods[c];a[d]=a.factory(d)}a.load=function(a){var b=document.createElement(\"script\");\nb.type=\"text\/javascript\";b.async=!0;b.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"tag.mtrcs.samba.tv\/v3\/tag\/\"+a+\"\/sambaTag.js\";a=document.getElementsByTagName(\"script\")[0];a.parentNode.insertBefore(b,a)};a.attrs||(a.attrs={});a.SNIPPET_VERSION=\"1.0.1\";a.load(\"vox\/vox-voxSubSection\")}}();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":139
    }],
  "predicates":[{
      "function":"_eq",
      "arg0":["macro",7],
      "arg1":"true"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"gtm.linkClick"
    },{
      "function":"_eq",
      "arg0":["macro",5],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",2],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",17],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",36],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",37],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",38],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"social"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"gtm.js"
    },{
      "function":"_re",
      "arg0":["macro",39],
      "arg1":".*"
    },{
      "function":"_eq",
      "arg0":["macro",0],
      "arg1":"true"
    },{
      "function":"_cn",
      "arg0":["macro",39],
      "arg1":"apps.voxmedia.com\/"
    },{
      "function":"_eq",
      "arg0":["macro",63],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"virtualPageView"
    },{
      "function":"_cn",
      "arg0":["macro",66],
      "arg1":"newsletter"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"gtm.formSubmit"
    },{
      "function":"_cn",
      "arg0":["macro",3],
      "arg1":"newsletter"
    },{
      "function":"_eq",
      "arg0":["macro",4],
      "arg1":"link:related"
    },{
      "function":"_eq",
      "arg0":["macro",69],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",22],
      "arg1":"undefined"
    },{
      "function":"_cn",
      "arg0":["macro",64],
      "arg1":"facebook"
    },{
      "function":"_eq",
      "arg0":["macro",60],
      "arg1":"true"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"social-buttons__more"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"c-entry-update-bar__follow|rss"
    },{
      "function":"_re",
      "arg0":["macro",71],
      "arg1":"(^$|((^|,)1434782_130($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"recode|eater|verge|polygon"
    },{
      "function":"_cn",
      "arg0":["macro",39],
      "arg1":"sports-bar-chain-to-pay-6-8m-over-tip-skimming-and-wage-violations"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"theverge"
    },{
      "function":"_re",
      "arg0":["macro",23],
      "arg1":"Post|Preview"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"gtm.click"
    },{
      "function":"_re",
      "arg0":["macro",39],
      "arg1":"la.curbed.com\/maps\/things-to-do-kids-los-angeles"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"recirc-module__link"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"css-vote-button"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"pds-return-poll"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"css-view-results"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"curbed"
    },{
      "function":"_re",
      "arg0":["macro",66],
      "arg1":"city-boxes|city-title",
      "ignore_case":true
    },{
      "function":"_eq",
      "arg0":["macro",74],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",75],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"analyticsEvent"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"GAEvent"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"embedPageView"
    },{
      "function":"_cn",
      "arg0":["macro",39],
      "arg1":"apps.voxmedia.com"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"gtm.load"
    },{
      "function":"_cn",
      "arg0":["macro",39],
      "arg1":"\/ad\/"
    },{
      "function":"_eq",
      "arg0":["macro",19],
      "arg1":"true"
    },{
      "function":"_cn",
      "arg0":["macro",73],
      "arg1":"eater"
    },{
      "function":"_cn",
      "arg0":["macro",73],
      "arg1":"polygon"
    },{
      "function":"_cn",
      "arg0":["macro",73],
      "arg1":"verge"
    },{
      "function":"_cn",
      "arg0":["macro",73],
      "arg1":"recode"
    },{
      "function":"_eq",
      "arg0":["macro",28],
      "arg1":"homepage"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"eater"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"vox"
    },{
      "function":"_re",
      "arg0":["macro",39],
      "arg1":"www.vox.com\/$|vox--com.sbn.sb8.k8s.sbndev.net\/|vox--com.sbn.sb8.k8s.sbndev.net\/culture\/2019\/1\/6\/18171367\/golden-globes-2019-regina-king-acceptance-speech|www.vox.com\/2014\/12\/31\/7474935\/best-movies-2014",
      "ignore_case":true
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"polygon"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"recode"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"sbnation"
    },{
      "function":"_re",
      "arg0":["macro",73],
      "arg1":"theringer"
    },{
      "function":"_re",
      "arg0":["macro",39],
      "arg1":"www.vox.com\/culture",
      "ignore_case":true
    }],
  "rules":[
    [["if",0,1],["add",16]],
    [["if",8],["unless",5,6,7],["add",2]],
    [["if",9],["add",17,18,28,29,30,31]],
    [["if",9,10],["unless",11,12],["add",19,21,32,34]],
    [["if",14],["unless",13],["add",3,34]],
    [["if",15,16],["add",0]],
    [["if",16,17],["add",0]],
    [["if",1,18],["add",20]],
    [["if",1],["unless",2],["add",4],["block",16]],
    [["if",1],["unless",3],["add",13],["block",16]],
    [["if",1],["unless",3,4],["add",14],["block",16,13]],
    [["if",1],["unless",20],["add",22]],
    [["if",9,10,21],["unless",11],["add",15],["block",34]],
    [["if",1,23],["add",5]],
    [["if",1,24,25],["add",6]],
    [["if",9],["unless",11,26],["add",23]],
    [["if",9,27],["add",24]],
    [["if",9,28],["add",25]],
    [["if",29,30],["add",1]],
    [["if",9,31],["add",26]],
    [["if",1,32],["add",7]],
    [["if",1,33],["add",8]],
    [["if",1,34],["add",9]],
    [["if",1,35],["add",10]],
    [["if",9,36],["add",27]],
    [["if",30,37],["add",11]],
    [["if",40],["unless",38,39],["add",12]],
    [["if",41],["add",12]],
    [["if",42],["add",32]],
    [["if",44],["unless",11,43],["add",33]],
    [["if",44,46],["add",35]],
    [["if",9,47],["unless",11],["add",36]],
    [["if",9,48],["unless",11],["add",37]],
    [["if",9,49],["unless",11],["add",38]],
    [["if",9,50],["unless",11],["add",39]],
    [["if",9,36,51],["add",40]],
    [["if",9,51,52],["add",41]],
    [["if",9,53,54],["add",42]],
    [["if",9,51,55],["add",43]],
    [["if",9,51,56],["add",44]],
    [["if",9,51,57],["add",45]],
    [["if",9,51,58],["add",46]],
    [["if",9,28,51],["add",47]],
    [["if",9,53,59],["add",48]],
    [["if",9,19],["block",21]],
    [["if",9,22],["block",15,32]],
    [["if",44,45],["block",33]]]
},
"runtime":[
[],[]
]


};
var aa,ca=this,da=/^[\w+/_-]+[=]{0,2}$/,ea=null;var fa=function(){},ha=function(a){return"function"==typeof a},ia=function(a){return"string"==typeof a},ja=function(a){return"number"==typeof a&&!isNaN(a)},ka=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},la=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},ma=function(a,b){if(a&&ka(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},oa=function(a,b){if(!ja(a)||
!ja(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},pa=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},qa=function(a){return Math.round(Number(a))||0},ra=function(a){return"false"==String(a).toLowerCase()?!1:!!a},sa=function(a){var b=[];if(ka(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},ta=function(a){return a?a.replace(/^\s+|\s+$/g,""):""},ua=function(){return(new Date).getTime()},va=function(){this.prefix="gtm.";this.values=
{}};va.prototype.set=function(a,b){this.values[this.prefix+a]=b};va.prototype.get=function(a){return this.values[this.prefix+a]};va.prototype.contains=function(a){return void 0!==this.get(a)};
var wa=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},xa=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},ya=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},za=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Aa=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Ba=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Ca=function(a){if(null==a)return String(a);var b=Ba.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Da=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Ea=function(a){if(!a||"object"!=Ca(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Da(a,"constructor")&&!Da(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Da(a,b)},Fa=function(a,b){var c=b||("array"==Ca(a)?[]:{}),d;for(d in a)if(Da(a,d)){var e=a[d];"array"==Ca(e)?("array"!=Ca(c[d])&&(c[d]=[]),c[d]=Fa(e,c[d])):Ea(e)?(Ea(c[d])||(c[d]={}),c[d]=Fa(e,c[d])):c[d]=e}return c};var f=window,u=document,Ga=navigator,Ha=u.currentScript&&u.currentScript.src,Ia=function(a,b){var c=f[a];f[a]=void 0===c?b:c;return f[a]},Ja=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},Ka=function(a,b,c){var d=u.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;Ja(d,b);c&&(d.onerror=c);var e;if(null===ea)b:{var g=ca.document,h=g.querySelector&&g.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&da.test(k)){ea=k;break b}}ea=""}e=ea;e&&d.setAttribute("nonce",e);var l=u.getElementsByTagName("script")[0]||u.body||u.head;l.parentNode.insertBefore(d,l);return d},La=function(){if(Ha){var a=Ha.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},Ma=function(a,b){var c=u.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=u.body&&u.body.lastChild||
u.body||u.head;d.parentNode.insertBefore(c,d);Ja(c,b);void 0!==a&&(c.src=a);return c},Na=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},Oa=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},Pa=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},z=function(a){f.setTimeout(a,0)},Ra=function(a){var b=
u.getElementById(a);if(b&&Qa(b,"id")!=a)for(var c=1;c<document.all[a].length;c++)if(Qa(document.all[a][c],"id")==a)return document.all[a][c];return b},Qa=function(a,b){return a&&b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},Sa=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},Ta=function(a){var b=u.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=
[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},Ua=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var g=a,h=0;g&&h<=c;h++){if(d[String(g.tagName).toLowerCase()])return g;g=g.parentElement}return null};var Va=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var Xa=/:[0-9]+$/,Ya=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var g=d[e].split("=");if(decodeURIComponent(g[0]).replace(/\+/g," ")===b){var h=g.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},ab=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=Za(a.protocol)||Za(f.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:f.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||f.location.hostname).replace(Xa,"").toLowerCase());var g=b,h,k=Za(a.protocol);g&&(g=String(g).toLowerCase());switch(g){case "url_no_fragment":h=$a(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace(Xa,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":h="/"==a.pathname.substr(0,1)?a.pathname:"/"+a.pathname;var m=h.split("/");0<=
la(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=Ya(h,e,void 0));break;case "extension":var n=a.pathname.split(".");h=1<n.length?n[n.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},Za=function(a){return a?a.replace(":","").toLowerCase():""},$a=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},bb=function(a){var b=u.createElement("a");
a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(c="/"+c);var d=b.hostname.replace(Xa,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};var cb=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),g=0;g<e.length;g++){var h=e[g].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},fb=function(a,b,c,d){var e=db(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=eb(e,function(g){return g.yb},b);if(1===e.length)return e[0].id;e=eb(e,function(g){return g.Sa},c);return e[0]?e[0].id:void 0}};
function hb(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=cb(b,e).indexOf(c)}
var kb=function(a,b,c,d,e,g){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{g&&(l=encodeURIComponent(l));var n=l;n&&1200<n.length&&(n=n.substring(0,1200));l=n;m=a+"="+l}var p=void 0,t=void 0,q;for(q in h)if(h.hasOwnProperty(q)){var r=h[q];if(null!=r)switch(q){case "secure":r&&(m+="; secure");break;case "domain":p=r;break;default:"path"==q&&(t=r),"expires"==q&&r instanceof Date&&(r=
r.toUTCString()),m+="; "+q+"="+r}}if("auto"===p){for(var v=ib(),x=0;x<v.length;++x){var y="none"!=v[x]?v[x]:void 0;if(!jb(y,t)&&hb(m+(y?"; domain="+y:""),a,l)){k=!0;break a}}k=!1}else p&&"none"!=p&&(m+="; domain="+p),k=!jb(p,t)&&hb(m,a,l)}return k};function eb(a,b,c){for(var d=[],e=[],g,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===g||l<g?(e=[k],g=l):l===g&&e.push(k)}return 0<d.length?d:e}
function db(a,b){for(var c=[],d=cb(a),e=0;e<d.length;e++){var g=d[e].split("."),h=g.shift();if(!b||-1!==b.indexOf(h)){var k=g.shift();k&&(k=k.split("-"),c.push({id:g.join("."),yb:1*k[0]||1,Sa:1*k[1]||1}))}}return c}
var lb=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,mb=/(^|\.)doubleclick\.net$/i,jb=function(a,b){return mb.test(document.location.hostname)||"/"===b&&lb.test(a)},ib=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));a.push("none");return a};
var nb=[],ob={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},pb=function(a){return ob[a]},rb=/[\x00\x22\x26\x27\x3c\x3e]/g;var vb=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,wb={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},xb=function(a){return wb[a]};
nb[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(vb,xb)+"'"}};var Gb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,Hb={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},Ib=function(a){return Hb[a]};nb[16]=function(a){return a};var Kb=[],Lb=[],Mb=[],Nb=[],Ob=[],Pb={},Qb,Rb,Sb,Tb=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},Ub=function(a){var b=a["function"];if(!b)throw Error("Error: No function name given for function call.");var c=!!Pb[b],d={},e;for(e in a)a.hasOwnProperty(e)&&0===e.indexOf("vtp_")&&(d[c?e:e.substr(4)]=a[e]);return c?Pb[b](d):(void 0)(b,d)},Wb=function(a,b,c,d){c=c||[];d=d||fa;var e={},g;for(g in a)a.hasOwnProperty(g)&&(e[g]=Vb(a[g],b,c,d));
return e},Xb=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=Pb[b];return c?c.priorityOverride||0:0},Vb=function(a,b,c,d){if(ka(a)){var e;switch(a[0]){case "function_id":return a[1];case "list":e=[];for(var g=1;g<a.length;g++)e.push(Vb(a[g],b,c,d));return e;case "macro":var h=a[1];if(c[h])return;var k=Kb[h];if(!k||b(k))return;c[h]=!0;try{var l=Wb(k,b,c,d);e=Ub(l);Sb&&(e=Sb.ff(e,l))}catch(y){d(y,h),e=!1}c[h]=!1;return e;case "map":e={};for(var m=
1;m<a.length;m+=2)e[Vb(a[m],b,c,d)]=Vb(a[m+1],b,c,d);return e;case "template":e=[];for(var n=!1,p=1;p<a.length;p++){var t=Vb(a[p],b,c,d);Rb&&(n=n||t===Rb.ob);e.push(t)}return Rb&&n?Rb.kf(e):e.join("");case "escape":e=Vb(a[1],b,c,d);if(Rb&&ka(a[1])&&"macro"===a[1][0]&&Rb.Nf(a))return Rb.Xf(e);e=String(e);for(var q=2;q<a.length;q++)nb[a[q]]&&(e=nb[a[q]](e));return e;case "tag":var r=a[1];if(!Nb[r])throw Error("Unable to resolve tag reference "+r+".");return e={wd:a[2],index:r};case "zb":var v={arg0:a[2],
arg1:a[3],ignore_case:a[5]};v["function"]=a[1];var x=Yb(v,b,c,d);a[4]&&(x=!x);return x;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},Yb=function(a,b,c,d){try{return Qb(Wb(a,b,c,d))}catch(e){JSON.stringify(a)}return null};var Zb=function(){var a=function(b){return{toString:function(){return b}}};return{Uc:a("convert_case_to"),Vc:a("convert_false_to"),Wc:a("convert_null_to"),Xc:a("convert_true_to"),Yc:a("convert_undefined_to"),ra:a("function"),ye:a("instance_name"),ze:a("live_only"),Ae:a("malware_disabled"),Bg:a("original_vendor_template_id"),Be:a("once_per_event"),md:a("once_per_load"),nd:a("setup_tags"),Ce:a("tag_id"),od:a("teardown_tags")}}();var $b=null,cc=function(a,b){function c(t){for(var q=0;q<t.length;q++)e[t[q]]=!0}var d=[],e=[];$b=ac(a,b||function(){});for(var g=0;g<Lb.length;g++){var h=Lb[g],k=bc(h);if(k){for(var l=h.add||[],m=0;m<l.length;m++)d[l[m]]=!0;c(h.block||[])}else null===k&&c(h.block||[])}for(var n=[],p=0;p<Nb.length;p++)d[p]&&!e[p]&&(n[p]=!0);return n},bc=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=$b(b[c]);if(!d)return null===d?null:!1}for(var e=a.unless||[],g=0;g<e.length;g++){var h=$b(e[g]);if(null===
h)return null;if(h)return!1}return!0},ac=function(a,b){var c=[];return function(d){void 0===c[d]&&(c[d]=Yb(Mb[d],a,void 0,b));return c[d]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */
var rc={},sc=null,tc=Math.random();rc.m="GTM-W8JKW6";rc.sb="430";var uc="www.googletagmanager.com/gtm.js";var vc=uc,wc=null,xc=null,yc=null,zc="//www.googletagmanager.com/a?id="+rc.m+"&cv=113",Ac={},Bc={},Cc=function(){var a=sc.sequence||0;sc.sequence=a+1;return a};var E=function(a,b,c,d){return(2===Dc()||d||"http:"!=f.location.protocol?a:b)+c},Dc=function(){var a=La(),b;if(1===a)a:{var c=vc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,g=1,h=u.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===g&&0===l.indexOf(d)&&(g=2)}}b=g}else b=a;return b};var Ec=!1;var Ic={},Jc=function(a){Ic.GTM=Ic.GTM||[];Ic.GTM[a]=!0};
var Kc=function(){return"&tc="+Nb.filter(function(a){return a}).length},Tc=function(){Lc&&(f.clearTimeout(Lc),Lc=void 0);void 0===Mc||Nc[Mc]&&!Oc||(Pc[Mc]||Qc.Pf()||0>=Rc--?(Jc(1),Pc[Mc]=!0):(Qc.fg(),Na(Sc()),Nc[Mc]=!0,Oc=""))},Sc=function(){var a=Mc;if(void 0===a)return"";for(var b,c=[],d=Ic.GTM||[],e=0;e<d.length;e++)d[e]&&(c[Math.floor(e/6)]^=1<<e%6);for(var g=0;g<c.length;g++)c[g]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(c[g]||0);b=c.join("");return[Uc,Nc[a]?"":
"&es=1",Vc[a],b?"&u="+b:"",Kc(),Oc,"&z=0"].join("")},Wc=function(){return[zc,"&v=3&t=t","&pid="+oa(),"&rv="+rc.sb].join("")},Xc="0.005000">Math.random(),Uc=Wc(),Yc=function(){Uc=Wc()},Nc={},Oc="",Mc=void 0,Vc={},Pc={},Lc=void 0,Qc=function(a,b){var c=0,d=0;return{Pf:function(){if(c<a)return!1;ua()-d>=b&&(c=0);return c>=a},fg:function(){ua()-d>=b&&(c=0);c++;d=ua()}}}(2,1E3),Rc=1E3,Zc=function(a,b){if(Xc&&!Pc[a]&&Mc!==a){Tc();Mc=a;Oc="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):
"*";Vc[a]="&e="+c+"&eid="+a;Lc||(Lc=f.setTimeout(Tc,500))}},$c=function(a,b,c){if(Xc&&!Pc[a]&&b){a!==Mc&&(Tc(),Mc=a);var d=c+String(b[Zb.ra]||"").replace(/_/g,"");Oc=Oc?Oc+"."+d:"&tr="+d;Lc||(Lc=f.setTimeout(Tc,500));2022<=Sc().length&&Tc()}};var ad=new va,bd={},cd={},gd={name:"dataLayer",set:function(a,b){Fa(dd(a,b),bd);ed()},get:function(a){return fd(a,2)},reset:function(){ad=new va;bd={};ed()}},fd=function(a,b){if(2!=b){var c=ad.get(a);if(Xc){var d=hd(a);c!==d&&Jc(5)}return c}return hd(a)},hd=function(a,b,c){var d=a.split("."),e=!1,g=void 0;return e?g:jd(d)},jd=function(a){for(var b=bd,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var md=function(a,b){cd.hasOwnProperty(a)||(ad.set(a,b),Fa(dd(a,b),bd),ed())},dd=function(a,b){for(var c={},d=c,e=a.split("."),g=0;g<e.length-1;g++)d=d[e[g]]={};d[e[e.length-1]]=b;return c},ed=function(a){pa(cd,function(b,c){ad.set(b,c);Fa(dd(b,void 0),bd);Fa(dd(b,c),bd);a&&delete cd[b]})};var nd=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),od={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},pd={cl:["ecl"],customPixels:["customScripts","html"],
ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts"],customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]};
var rd=function(a){var b=fd("gtm.whitelist");b&&Jc(9);var c=b&&Aa(sa(b),od),d=fd("gtm.blacklist");d||(d=fd("tagTypeBlacklist"))&&Jc(3);d?Jc(8):d=[];
qd()&&(d=sa(d),d.push("nonGooglePixels","nonGoogleScripts"));0<=la(sa(d),"google")&&Jc(2);var e=d&&Aa(sa(d),pd),g={};return function(h){var k=h&&h[Zb.ra];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==g[k])return g[k];var l=Bc[k]||[],m=a(k);if(b){var n;if(n=m)a:{if(0>la(c,k))if(l&&0<l.length)for(var p=0;p<l.length;p++){if(0>la(c,l[p])){Jc(11);
n=!1;break a}}else{n=!1;break a}n=!0}m=n}var t=!1;if(d){var q=0<=la(e,k);if(q)t=q;else{var r;b:{for(var v=l||[],x=new va,y=0;y<e.length;y++)x.set(e[y],!0);for(var w=0;w<v.length;w++)if(x.get(v[w])){r=!0;break b}r=!1}var B=r;B&&Jc(10);t=B}}return g[k]=!m||t}},qd=function(){return nd.test(f.location&&f.location.hostname)};var sd={ff:function(a,b){b[Zb.Uc]&&"string"===typeof a&&(a=1==b[Zb.Uc]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(Zb.Wc)&&null===a&&(a=b[Zb.Wc]);b.hasOwnProperty(Zb.Yc)&&void 0===a&&(a=b[Zb.Yc]);b.hasOwnProperty(Zb.Xc)&&!0===a&&(a=b[Zb.Xc]);b.hasOwnProperty(Zb.Vc)&&!1===a&&(a=b[Zb.Vc]);return a}};var td={active:!0,isWhitelisted:function(){return!0}},ud=function(a){var b=sc.zones;!b&&a&&(b=sc.zones=a());return b};var vd=!1,wd=0,xd=[];function yd(a){if(!vd){var b=u.createEventObject,c="complete"==u.readyState,d="interactive"==u.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){vd=!0;for(var e=0;e<xd.length;e++)z(xd[e])}xd.push=function(){for(var g=0;g<arguments.length;g++)z(arguments[g]);return 0}}}function zd(){if(!vd&&140>wd){wd++;try{u.documentElement.doScroll("left"),yd()}catch(a){f.setTimeout(zd,50)}}}var Ad=function(a){vd?a():xd.push(a)};var Bd=function(){function a(d){return!ja(d)||0>d?0:d}if(!sc._li&&f.performance&&f.performance.timing){var b=f.performance.timing.navigationStart,c=ja(gd.get("gtm.start"))?gd.get("gtm.start"):0;sc._li={cst:a(c-b),cbt:a(xc-b)}}};var Fd=!1,Gd=function(){return f.GoogleAnalyticsObject&&f[f.GoogleAnalyticsObject]},Hd=!1;
var Id=function(a){f.GoogleAnalyticsObject||(f.GoogleAnalyticsObject=a||"ga");var b=f.GoogleAnalyticsObject;if(f[b])f.hasOwnProperty(b)||Jc(12);else{var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);f[b]=c}Bd();return f[b]},Jd=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=Gd();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var Ld=function(){},Kd=function(){return f.GoogleAnalyticsObject||"ga"},Md=!1;var Td=function(a){};function Sd(a,b){a.containerId=rc.m;var c={type:"GENERIC",value:a};b.length&&(c.trace=b);return c};function Ud(a,b,c,d,e){var g=Nb[a],h=Vd(a,b,c,d,e);if(!h)return null;var k=Vb(g[Zb.nd],d.fa,[],fa);if(k&&k.length){var l=k[0];h=Ud(l.index,b,{I:h,O:1===l.wd?c.terminate:h,terminate:c.terminate},d,e)}return h}
function Vd(a,b,c,d,e){function g(){if(h[Zb.Ae])l();else{var y=Wb(h,d.fa,[],function(B){Jc(6);Td(B)}),w=!1;y.vtp_gtmOnSuccess=function(){if(!w){w=!0;$c(d.id,Nb[a],"5");k()}};y.vtp_gtmOnFailure=function(){if(!w){w=!0;$c(d.id,Nb[a],"6");l()}};y.vtp_gtmTagId=h.tag_id;$c(d.id,h,"1");try{Ub(y)}catch(B){Td(B);$c(d.id,h,"7");w||(w=!0,l())}}}var h=Nb[a],k=c.I,l=c.O,m=c.terminate;if(d.fa(h))return null;var n=Vb(h[Zb.od],d.fa,[],fa);if(n&&n.length){var p=n[0],t=Ud(p.index,b,{I:k,O:l,terminate:m},d,e);if(!t)return null;
k=t;l=2===p.wd?m:t}if(h[Zb.md]||h[Zb.Be]){var q=h[Zb.md]?Ob:b,r=k,v=l;if(!q[a]){g=xa(g);var x=Wd(a,q,g);k=x.I;l=x.O}return function(){q[a](r,v)}}return g}function Wd(a,b,c){var d=[],e=[];b[a]=Xd(d,e,c);return{I:function(){b[a]=Yd;for(var g=0;g<d.length;g++)d[g]()},O:function(){b[a]=Zd;for(var g=0;g<e.length;g++)e[g]()}}}function Xd(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function Yd(a){a()}function Zd(a,b){b()};function $d(a){var b=0,c=0,d=!1;return{add:function(){c++;return xa(function(){b++;d&&b>=c&&a()})},Qe:function(){d=!0;b>=c&&a()}}}var ce=function(a){for(var b=$d(a.callback),c=[],d=[],e=0;e<Nb.length;e++)if(a.Ua[e]){var g=Nb[e];var h=b.add();try{var k=Ud(e,c,{I:h,O:h,terminate:h},a,e);k?d.push({Wd:e,b:Xb(g),uf:k}):(ae(e,a),h())}catch(m){h()}}b.Qe();d.sort(be);for(var l=0;l<d.length;l++)d[l].uf();return 0<d.length};
function be(a,b){var c,d=b.b,e=a.b;c=d>e?1:d<e?-1:0;var g;if(0!==c)g=c;else{var h=a.Wd,k=b.Wd;g=h>k?1:h<k?-1:0}return g}function ae(a,b){if(!Xc)return;var c=function(d){var e=b.fa(Nb[d])?"3":"4",g=Vb(Nb[d][Zb.nd],b.fa,[],fa);g&&g.length&&c(g[0].index);$c(b.id,Nb[d],e);var h=Vb(Nb[d][Zb.od],b.fa,[],fa);h&&h.length&&c(h[0].index)};c(a);}
var de=!1,ee=function(a,b,c,d){if("gtm.js"==b){if(de)return!1;de=!0}Zc(a,b);var e=rd(c),g={id:a,name:b,callback:d||fa,fa:e,Ua:[]};g.Ua=cc(e,function(n){Td(n)});var h=ce(g);"gtm.js"!==b&&"gtm.sync"!==b||Ld();if(!h)return h;for(var k={__cl:!0,__ecl:!0,__ehl:!0,__evl:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0},l=0;l<
g.Ua.length;l++)if(g.Ua[l]){var m=Nb[l];if(m&&!k[m[Zb.ra]])return!0}return!1};var F={Ob:"event_callback",Qb:"event_timeout"};var ge={};var he=/[A-Z]+/,ie=/\s/,je=function(a){if(ia(a)&&(a=ta(a),!ie.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(he.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],da:d}}}}},le=function(a){for(var b={},c=0;c<a.length;++c){var d=je(a[c]);d&&(b[d.id]=d)}ke(b);var e=[];pa(b,function(g,h){e.push(h)});return e};
function ke(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.da[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var me=null,ne={},oe={},qe,re=function(a,b){var c={event:a};b&&(c.eventModel=Fa(b),b[F.Ob]&&(c.eventCallback=b[F.Ob]),b[F.Qb]&&(c.eventTimeout=b[F.Qb]));return c};
var we={config:function(a){},event:function(a){var b=a[1];if(ia(b)&&!(3<a.length)){var c;if(2<
a.length){if(!Ea(a[2]))return;c=a[2]}var d=re(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(a){if(3===a.length){var b=a[1],c=a[2];ge[b]||(ge[b]=[]);ge[b].push(c)}},set:function(a){var b;2==a.length&&Ea(a[1])?b=Fa(a[1]):3==a.length&&ia(a[1])&&(b={},b[a[1]]=a[2]);if(b)return b.eventModel=Fa(b),b.event="gtag.set",b._clear=!0,b}},xe={policy:!0};var ye=function(){return!1};var Ee=function(a){if(De(a))return a;this.ug=a};Ee.prototype.Af=function(){return this.ug};var De=function(a){return!a||"object"!==Ca(a)||Ea(a)?!1:"getUntrustedUpdateValue"in a};Ee.prototype.getUntrustedUpdateValue=Ee.prototype.Af;var Fe=!1,Ge=[];function He(){if(!Fe){Fe=!0;for(var a=0;a<Ge.length;a++)z(Ge[a])}}var Ie=function(a){Fe?z(a):Ge.push(a)};var Je=[],Ke=!1;function Le(a){var b=a.eventCallback,c=xa(function(){ha(b)&&z(function(){b(rc.m)})}),d=a.eventTimeout;d&&f.setTimeout(c,Number(d));return c}
var Me=function(a){return f["dataLayer"].push(a)},Oe=function(a){var b=a._clear;pa(a,function(g,h){"_clear"!==g&&(b&&md(g,void 0),md(g,h))});var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=Cc(),a["gtm.uniqueEventId"]=d,md("gtm.uniqueEventId",d));yc=c;var e=Ne(a);yc=null;if(!wc){wc=a["gtm.start"];}return e};
function Ne(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=sc.zones;d=e?e.checkState(rc.m,c):td;if(!d.active)return!1;var g=Le(a);return ee(c,b,d.isWhitelisted,g)?!0:!1}
var Pe=function(){for(var a=!1;!Ke&&0<Je.length;){Ke=!0;delete bd.eventModel;ed();var b=Je.shift();if(null!=b){var c=De(b);if(c){var d=b;b=De(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],g=0;g<e.length;g++){var h=e[g],k=fd(h,1);if(ka(k)||Ea(k))k=Fa(k);cd[h]=k}}try{if(ha(b))try{b.call(gd)}catch(v){}else if(ka(b)){var l=b;if(ia(l[0])){var m=
l[0].split("."),n=m.pop(),p=l.slice(1),t=fd(m.join("."),2);if(void 0!==t&&null!==t)try{t[n].apply(t,p)}catch(v){}}}else{var q=b;if(q&&("[object Arguments]"==Object.prototype.toString.call(q)||Object.prototype.hasOwnProperty.call(q,"callee"))){a:{if(b.length&&ia(b[0])){var r=we[b[0]];if(r&&(!c||!xe[b[0]])){b=r(b);break a}}b=void 0}if(!b){Ke=!1;continue}}a=Oe(b)||a}}finally{c&&ed(!0)}}Ke=!1}
return!a},Qe=function(){var a=Pe();try{var b=rc.m,c=f["dataLayer"].hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}}catch(g){}return a},Re=function(){var a=Ia("dataLayer",[]),b=Ia("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};Ad(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});Ie(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});var c=a.push;a.push=function(){var d;
if(0<sc.SANDBOXED_JS_SEMAPHORE){d=[];for(var e=0;e<arguments.length;e++)d[e]=new Ee(arguments[e])}else d=[].slice.call(arguments,0);var g=c.apply(a,d);Je.push.apply(Je,d);if(300<this.length)for(Jc(4);300<this.length;)this.shift();var h="boolean"!==typeof g||g;return Pe()&&h};Je.push.apply(Je,a.slice(0));z(Qe)};var Te=function(a){return Se?u.querySelectorAll(a):null},Ue=function(a,b){if(!Se)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!u.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},Ve=!1;if(u.querySelectorAll)try{var We=u.querySelectorAll(":root");We&&1==We.length&&We[0]==u.documentElement&&(Ve=!0)}catch(a){}var Se=Ve;var Xe;var tf={};tf.ob=new String("undefined");
var uf=function(a){this.resolve=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===tf.ob?b:a[d]);return c.join("")}};uf.prototype.toString=function(){return this.resolve("undefined")};uf.prototype.valueOf=uf.prototype.toString;tf.De=uf;tf.$b={};tf.kf=function(a){return new uf(a)};var vf={};tf.gg=function(a,b){var c=Cc();vf[c]=[a,b];return c};tf.td=function(a){var b=a?0:1;return function(c){var d=vf[c];if(d&&"function"===typeof d[b])d[b]();vf[c]=void 0}};tf.Nf=function(a){for(var b=!1,c=!1,
d=2;d<a.length;d++)b=b||8===a[d],c=c||16===a[d];return b&&c};tf.Xf=function(a){if(a===tf.ob)return a;var b=Cc();tf.$b[b]=a;return'google_tag_manager["'+rc.m+'"].macro('+b+")"};tf.Rf=function(a,b,c){a instanceof tf.De&&(a=a.resolve(tf.gg(b,c)),b=fa);return{nc:a,I:b}};var wf=function(a,b,c){var d={event:b,"gtm.element":a,"gtm.elementClasses":a.className,"gtm.elementId":a["for"]||Qa(a,"id")||"","gtm.elementTarget":a.formTarget||a.target||""};c&&(d["gtm.triggers"]=c.join(","));d["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||a.href||a.src||a.code||a.codebase||"";return d},xf=function(a){sc.hasOwnProperty("autoEventsSettings")||(sc.autoEventsSettings={});var b=sc.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},
yf=function(a,b,c){xf(a)[b]=c},zf=function(a,b,c,d){var e=xf(a),g=wa(e,b,d);e[b]=c(g)},Af=function(a,b,c){var d=xf(a);return wa(d,b,c)};var Bf=function(){for(var a=Ga.userAgent+(u.cookie||"")+(u.referrer||""),b=a.length,c=f.history.length;0<c;)a+=c--^b++;var d=1,e,g,h;if(a)for(d=0,g=a.length-1;0<=g;g--)h=a.charCodeAt(g),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(ua()/1E3)].join(".")},Ef=function(a,b,c,d){var e=Cf(b);return fb(a,e,Df(c),d)},Cf=function(a){if(!a)return 1;a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},Df=function(a){if(!a||
"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};function Ff(a,b){var c=""+Cf(a),d=Df(b);1<d&&(c+="-"+d);return c};var Gf=["1"],Hf={},Lf=function(a,b,c,d){var e=If(a);Hf[e]||Jf(e,b,c)||(Kf(e,Bf(),b,c,d),Jf(e,b,c))};function Kf(a,b,c,d,e){var g;g=["1",Ff(d,c),b].join(".");kb(a,g,c,d,0==e?void 0:new Date(ua()+1E3*(void 0==e?7776E3:e)))}function Jf(a,b,c){var d=Ef(a,b,c,Gf);d&&(Hf[a]=d);return d}function If(a){return(a||"_gcl")+"_au"};var Mf=function(){for(var a=[],b=u.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({Mc:e[1],value:e[2]})}var g={};if(!a||!a.length)return g;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(g[a[h].Mc]||(g[a[h].Mc]=[]),g[a[h].Mc].push({timestamp:k[1],xf:k[2]}))}return g};function Nf(){for(var a=Of,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function Pf(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}
var Of,Qf,Rf=function(a){Of=Of||Pf();Qf=Qf||Nf();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,g=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=g>>2,m=(g&3)<<4|h>>4,n=(h&15)<<2|k>>6,p=k&63;e||(p=64,d||(n=64));b.push(Of[l],Of[m],Of[n],Of[p])}return b.join("")},Sf=function(a){function b(l){for(;d<a.length;){var m=a.charAt(d++),n=Qf[m];if(null!=n)return n;if(!/^[\s\xa0]*$/.test(m))throw Error("Unknown base64 encoding at char: "+m);}return l}Of=Of||Pf();Qf=Qf||
Nf();for(var c="",d=0;;){var e=b(-1),g=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|g>>4);64!=h&&(c+=String.fromCharCode(g<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var Tf;function Uf(a,b){if(!a||b===u.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}var Vf=function(){var a=Ia("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var Wf=/(.*?)\*(.*?)\*(.*)/,Xf=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,Yf=/^(?:www\.|m\.|amp\.)+/,Zf=/([^?#]+)(\?[^#]*)?(#.*)?/,$f=/(.*?)(^|&)_gl=([^&]*)&?(.*)/,bg=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(Rf(String(d))))}var e=b.join("*");return["1",ag(e),e].join("*")},ag=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||
window.navigator.language,Math.floor((new Date).getTime()/60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=Tf)){for(var e=Array(256),g=0;256>g;g++){for(var h=g,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[g]=h}d=e}Tf=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^Tf[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},dg=function(){return function(a){var b=bb(f.location.href),c=b.search.replace("?",""),d=Ya(c,"_gl",!0)||"";a.query=cg(d)||{};var e=ab(b,"fragment").match($f);a.fragment=cg(e&&e[3]||
"")||{}}},cg=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var g=Wf.exec(d);if(g){c=g;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],n=0;n<b;++n)if(m===ag(k,n)){l=!0;break a}l=!1}if(l){for(var p={},t=k?k.split("*"):[],q=0;q<t.length;q+=2)p[t[q]]=Sf(t[q+1]);return p}}}}catch(r){}};
function eg(a,b,c){function d(m){var n=m,p=$f.exec(n),t=n;if(p){var q=p[2],r=p[4];t=p[1];r&&(t=t+q+r)}m=t;var v=m.charAt(m.length-1);m&&"&"!==v&&(m+="&");return m+l}c=void 0===c?!1:c;var e=Zf.exec(b);if(!e)return"";var g=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+g+h+k}
function fg(a,b,c){for(var d={},e={},g=Vf().decorators,h=0;h<g.length;++h){var k=g[h];(!c||k.forms)&&Uf(k.domains,b)&&(k.fragment?ya(e,k.callback()):ya(d,k.callback()))}if(za(d)){var l=bg(d);if(c){if(a&&a.action){var m=(a.method||"").toLowerCase();if("get"===m){for(var n=a.childNodes||[],p=!1,t=0;t<n.length;t++){var q=n[t];if("_gl"===q.name){q.setAttribute("value",l);p=!0;break}}if(!p){var r=u.createElement("input");r.setAttribute("type","hidden");r.setAttribute("name","_gl");r.setAttribute("value",
l);a.appendChild(r)}}else if("post"===m){var v=eg(l,a.action);Va.test(v)&&(a.action=v)}}}else gg(l,a,!1)}if(!c&&za(e)){var x=bg(e);gg(x,a,!0)}}function gg(a,b,c){if(b.href){var d=eg(a,b.href,void 0===c?!1:c);Va.test(d)&&(b.href=d)}}
var hg=function(a){try{var b;a:{for(var c=a.target||a.srcElement||{},d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var g=e.protocol;"http:"!==g&&"https:"!==g||fg(e,e.hostname,!1)}}catch(h){}},ig=function(a){try{var b=a.target||a.srcElement||{};if(b.action){var c=ab(bb(b.action),"host");fg(b,c,!0)}}catch(d){}},jg=function(a,b,c,d){var e=Vf();e.init||(Oa(u,"mousedown",hg),Oa(u,"keyup",hg),Oa(u,"submit",ig),e.init=!0);var g={callback:a,
domains:b,fragment:"fragment"===c,forms:!!d};Vf().decorators.push(g)},kg=function(){var a=u.location.hostname,b=Xf.exec(u.referrer);if(!b)return!1;var c=b[2],d=b[1],e="";if(c){var g=c.split("/"),h=g[1];e="s"===h?decodeURIComponent(g[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}return a.replace(Yf,"")===e.replace(Yf,"")},lg=function(a,b){return!1===a?!1:a||b||kg()};var mg=/^\w+$/,ng=/^[\w-]+$/,og=/^~?[\w-]+$/,pg={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha"};function qg(a){return a&&"string"==typeof a&&a.match(mg)?a:"_gcl"}var sg=function(){var a=bb(f.location.href),b=ab(a,"query",!1,void 0,"gclid"),c=ab(a,"query",!1,void 0,"gclsrc"),d=ab(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||Ya(e,"gclid",void 0);c=c||Ya(e,"gclsrc",void 0)}return rg(b,c,d)};
function rg(a,b,c){var d={},e=function(g,h){d[h]||(d[h]=[]);d[h].push(g)};if(void 0!==a&&a.match(ng))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha")}c&&e(c,"dc");return d}
function tg(a,b,c){function d(p,t){var q=ug(p,e);q&&kb(q,t,h,g,l,!0)}b=b||{};var e=qg(b.prefix),g=b.domain||"auto",h=b.path||"/",k=void 0==b.Jd?7776E3:b.Jd;c=c||ua();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),n=function(p){return["GCL",m,p].join(".")};a.aw&&(!0===b.$g?d("aw",n("~"+a.aw[0])):d("aw",n(a.aw[0])));a.dc&&d("dc",n(a.dc[0]));a.gf&&d("gf",n(a.gf[0]));a.ha&&d("ha",n(a.ha[0]))}
var ug=function(a,b){var c=pg[a];if(void 0!==c)return b+c},vg=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||0)};function wg(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var xg=function(a,b,c,d,e){if(ka(b)){var g=qg(e);jg(function(){for(var h={},k=0;k<a.length;++k){var l=ug(a[k],g);if(l){var m=cb(l,u.cookie);m.length&&(h[l]=m.sort()[m.length-1])}}return h},b,c,d)}},yg=function(a){return a.filter(function(b){return og.test(b)})},zg=function(a){for(var b=["aw","dc"],c=qg(a&&a.prefix),d={},e=0;e<b.length;e++)pg[b[e]]&&(d[b[e]]=pg[b[e]]);pa(d,function(g,h){var k=cb(c+h,u.cookie);if(k.length){var l=k[0],m=vg(l),n={};n[g]=[wg(l)];tg(n,a,m)}})};var Ag=/^\d+\.fls\.doubleclick\.net$/;function Bg(a){var b=bb(f.location.href),c=ab(b,"host",!1);if(c&&c.match(Ag)){var d=ab(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function Cg(a,b){if("aw"==a||"dc"==a){var c=Bg("gcl"+a);if(c)return c.split(".")}var d=qg(b);if("_gcl"==d){var e;e=sg()[a]||[];if(0<e.length)return e}var g=ug(a,d),h;if(g){var k=[];if(u.cookie){var l=cb(g,u.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var n=wg(l[m]);n&&-1===la(k,n)&&k.push(n)}h=yg(k)}else h=k}else h=k}else h=[];return h}
var Dg=function(){var a=Bg("gac");if(a)return decodeURIComponent(a);var b=Mf(),c=[];pa(b,function(d,e){for(var g=[],h=0;h<e.length;h++)g.push(e[h].xf);g=yg(g);g.length&&c.push(d+":"+g.join(","))});return c.join(";")},Eg=function(a,b,c,d,e){Lf(b,c,d,e);var g=Hf[If(b)],h=sg().dc||[],k=!1;if(g&&0<h.length){var l=sc.joined_au=sc.joined_au||{},m=b||"_gcl";if(!l[m])for(var n=0;n<h.length;n++){var p="https://adservice.google.com/ddm/regclk",t=p=p+"?gclid="+h[n]+"&auiddc="+g;Ga.sendBeacon&&Ga.sendBeacon(t)||Na(t);k=l[m]=
!0}}null==a&&(a=k);if(a&&g){var q=If(b),r=Hf[q];r&&Kf(q,r,c,d,e)}};var Fg;if(3===rc.sb.length)Fg="g";else{var Gg="G";Fg=Gg}
var Hg={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:Fg},Ig=function(a){var b=rc.m.split("-"),c=b[0].toUpperCase(),d=Hg[c]||"i",e=a&&"GTM"===c?b[1]:"",g;if(3===rc.sb.length){var h=void 0;g="2"+(h||"w")}else g="";return g+d+rc.sb+e};var Pg=!!f.MutationObserver,Qg=void 0,Rg=function(a){if(!Qg){var b=function(){var c=u.body;if(c)if(Pg)(new MutationObserver(function(){for(var e=0;e<Qg.length;e++)z(Qg[e])})).observe(c,{childList:!0,subtree:!0});else{var d=!1;Oa(c,"DOMNodeInserted",function(){d||(d=!0,z(function(){d=!1;for(var e=0;e<Qg.length;e++)z(Qg[e])}))})}};Qg=[];u.body?b():z(b)}Qg.push(a)};var ih=f.clearTimeout,jh=f.setTimeout,G=function(a,b,c){if(ye()){b&&z(b)}else return Ka(a,b,c)},kh=function(){return new Date},lh=function(){return f.location.href},mh=function(a){return ab(bb(a),"fragment")},nh=function(a){return $a(bb(a))},oh=function(a,b){return fd(a,b||2)},ph=function(a,b,c){b&&(a.eventCallback=b,c&&(a.eventTimeout=c));return Me(a)},qh=function(a,b){f[a]=b},L=function(a,b,c){b&&(void 0===f[a]||
c&&!f[a])&&(f[a]=b);return f[a]},rh=function(a,b,c){return cb(a,b,void 0===c?!0:!!c)},sh=function(a,b,c,d){var e={prefix:a,path:b,domain:c,Jd:d},g=sg();tg(g,e);zg(e)},th=function(a,b,c,d,e){var g=dg(),h=Vf();h.data||(h.data={query:{},fragment:{}},g(h.data));var k={},l=h.data;l&&(ya(k,l.query),ya(k,l.fragment));for(var m=qg(b),n=0;n<a.length;++n){var p=a[n];if(void 0!==pg[p]){var t=ug(p,m),q=k[t];if(q){var r=Math.min(vg(q),ua()),v;b:{for(var x=r,y=cb(t,u.cookie),
w=0;w<y.length;++w)if(vg(y[w])>x){v=!0;break b}v=!1}v||kb(t,q,c,d,0==e?void 0:new Date(r+1E3*(null==e?7776E3:e)),!0)}}}var B={prefix:b,path:c,domain:d};tg(rg(k.gclid,k.gclsrc),B);},uh=function(a,b,c,d,e){xg(a,b,c,d,e);},vh=function(a,b){if(ye()){b&&z(b)}else Ma(a,b)},wh=function(a){return!!Af(a,
"init",!1)},xh=function(a){yf(a,"init",!0)},yh=function(a,b,c){var d=(void 0===c?0:c)?"www.googletagmanager.com/gtag/js":vc;d+="?id="+encodeURIComponent(a)+"&l=dataLayer";b&&pa(b,function(e,g){g&&(d+="&"+e+"="+encodeURIComponent(g))});G(E("https://","http://",d))};
var Ah=tf.Rf;var Bh=new va;function Ch(a,b){function c(h){var k=bb(h),l=ab(k,"protocol"),m=ab(k,"host",!0),n=ab(k,"port"),p=ab(k,"path").toLowerCase().replace(/\/$/,"");if(void 0===l||"http"==l&&"80"==n||"https"==l&&"443"==n)l="web",n="default";return[l,m,n,p]}for(var d=c(String(a)),e=c(String(b)),g=0;g<d.length;g++)if(d[g]!==e[g])return!1;return!0}
function Dh(a){var b=a.arg0,c=a.arg1;if(a.any_of&&ka(c)){for(var d=0;d<c.length;d++)if(Dh({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=String(b).indexOf(String(c));case "_css":var e;a:{if(b){var g=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<g.length;h++)if(b[g[h]]){e=b[g[h]](c);break a}}catch(v){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-
l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var n;n=String(b).split(",");return 0<=la(n,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var p;var t=a.ignore_case?"i":void 0;try{var q=String(c)+t,r=Bh.get(q);r||(r=new RegExp(c,t),Bh.set(q,r));p=r.test(b)}catch(v){p=!1}return p;case "_sw":return 0==String(b).indexOf(String(c));
case "_um":return Ch(b,c)}return!1};var Fh=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var Gh={},Hh=encodeURI,M=encodeURIComponent,Ih=Na;var Jh=function(a,b){if(!a)return!1;var c=ab(bb(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var g=c.length-e.length;0<g&&"."!=e.charAt(0)&&(g--,e="."+e);if(0<=g&&c.indexOf(e,g)==g)return!0}}return!1};
var Kh=function(a,b,c){for(var d={},e=!1,g=0;a&&g<a.length;g++)a[g]&&a[g].hasOwnProperty(b)&&a[g].hasOwnProperty(c)&&(d[a[g][b]]=a[g][c],e=!0);return e?d:null};Gh.Of=function(){var a=!1;return a};var ti=function(a,b,c,d){this.n=a;this.t=b;this.p=c;this.d=d},ui=function(){this.c=1;this.e=[];this.p=null};function vi(a){var b=sc,c=b.gss=b.gss||{};return c[a]=c[a]||new ui}var wi=function(a,b){vi(a).p=b},xi=function(a){var b=vi(a),c=b.p;if(c){var d=b.e,e=[];b.e=[];var g=function(h){for(var k=0;k<h.length;k++)try{var l=h[k];l.d?(l.d=!1,e.push(l)):c(l.n,l.t,l.p)}catch(m){}};g(d);g(e)}};var zi=function(){var a=f.gaGlobal=f.gaGlobal||{};a.hid=a.hid||oa();return a.hid};var Oi=window,Pi=document,Qi=function(a){var b=Oi._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===Oi["ga-disable-"+a])return!0;try{var c=Oi.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(g){}for(var d=cb("AMP_TOKEN",Pi.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return Pi.getElementById("__gaOptOutExtension")?!0:!1};var Xi=function(a,b,c){Wi(a);var d=Math.floor(ua()/1E3);vi(a).e.push(new ti(b,d,c,void 0));xi(a)},Yi=function(a,b,c){Wi(a);var d=Math.floor(ua()/1E3);vi(a).e.push(new ti(b,d,c,!0))},Wi=function(a){if(1===vi(a).c){vi(a).c=2;var b=encodeURIComponent(a);Ka(("http:"!=f.location.protocol?"https:":"http:")+("//www.googletagmanager.com/gtag/js?id="+b+"&l=dataLayer&cx=c"))}},$i=function(a,b){},Zi=function(a,b){};var W={a:{}};
W.a.jsm=["customScripts"],function(){(function(a){W.__jsm=a;W.__jsm.g="jsm";W.__jsm.h=!0;W.__jsm.b=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=L("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();W.a.c=["google"],function(){(function(a){W.__c=a;W.__c.g="c";W.__c.h=!0;W.__c.b=0})(function(a){return a.vtp_value})}();
W.a.e=["google"],function(){(function(a){W.__e=a;W.__e.g="e";W.__e.h=!0;W.__e.b=0})(function(){return yc})}();
W.a.f=["google"],function(){(function(a){W.__f=a;W.__f.g="f";W.__f.h=!0;W.__f.b=0})(function(a){var b=oh("gtm.referrer",1)||u.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?ab(bb(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):nh(String(b)):String(b)})}();
W.a.cl=["google"],function(){function a(b){var c=b.target;if(c){var d=wf(c,"gtm.click");ph(d)}}(function(b){W.__cl=b;W.__cl.g="cl";W.__cl.h=!0;W.__cl.b=0})(function(b){if(!wh("cl")){var c=L("document");Oa(c,"click",a,!0);xh("cl")}z(b.vtp_gtmOnSuccess)})}();
W.a.j=["google"],function(){(function(a){W.__j=a;W.__j.g="j";W.__j.h=!0;W.__j.b=0})(function(a){for(var b=String(a.vtp_name).split("."),c=L(b.shift()),d=0;d<b.length;d++)c=c&&c[b[d]];return c})}();
W.a.u=["google"],function(){var a=function(b){return{toString:function(){return b}}};(function(b){W.__u=b;W.__u.g="u";W.__u.h=!0;W.__u.b=0})(function(b){var c;c=(c=b.vtp_customUrlSource?b.vtp_customUrlSource:oh("gtm.url",1))||lh();var d=b[a("vtp_component")];if(!d||"URL"==d)return nh(String(c));var e=bb(String(c)),g;if("QUERY"==d&&b[a("vtp_multiQueryKeys")])a:{var h=b[a("vtp_queryKey")],k;k=ka(h)?h:String(h||"").replace(/\s+/g,"").split(",");for(var l=0;l<k.length;l++){var m=ab(e,"QUERY",void 0,void 0,
k[l]);if(null!=m){g=m;break a}}g=void 0}else g=ab(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,"QUERY"==d?b[a("vtp_queryKey")]:void 0);return g})}();W.a.v=["google"],function(){(function(a){W.__v=a;W.__v.g="v";W.__v.h=!0;W.__v.b=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=oh(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();

W.a.ua=["google"],function(){var a,b=function(c){var d={},e={},g={},h={},k={};if(c.vtp_gaSettings){var l=c.vtp_gaSettings;Fa(Kh(l.vtp_fieldsToSet,"fieldName","value"),e);Fa(Kh(l.vtp_contentGroup,"index","group"),g);Fa(Kh(l.vtp_dimension,"index","dimension"),h);Fa(Kh(l.vtp_metric,"index","metric"),k);c.vtp_gaSettings=null;l.vtp_fieldsToSet=void 0;l.vtp_contentGroup=void 0;l.vtp_dimension=void 0;l.vtp_metric=void 0;var m=Fa(l);c=Fa(c,m)}Fa(Kh(c.vtp_fieldsToSet,"fieldName","value"),e);Fa(Kh(c.vtp_contentGroup,
"index","group"),g);Fa(Kh(c.vtp_dimension,"index","dimension"),h);Fa(Kh(c.vtp_metric,"index","metric"),k);var n=Id(c.vtp_functionName);if(ha(n)){var p="",t="";c.vtp_setTrackerName&&"string"==typeof c.vtp_trackerName?""!==c.vtp_trackerName&&(t=c.vtp_trackerName,p=t+"."):(t="gtm"+Cc(),p=t+".");var q={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,
legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},r={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0},v=function(K){var P=[].slice.call(arguments,0);P[0]=p+P[0];n.apply(window,P)},x=function(K,P){return void 0===P?P:K(P)},y=function(K,P){if(P)for(var ba in P)P.hasOwnProperty(ba)&&v("set",K+ba,P[ba])},w=function(){},B=function(K,P,ba){var na=0;if(K)for(var X in K)if(K.hasOwnProperty(X)&&(ba&&q[X]||!ba&&void 0===q[X])){var Z=r[X]?ra(K[X]):K[X];"anonymizeIp"!=X||Z||(Z=void 0);P[X]=Z;na++}return na},
A={name:t};B(e,A,!0);n("create",c.vtp_trackingId||d.trackingId,A);v("set","&gtm",Ig(!0));c.vtp_enableRecaptcha&&v("require","recaptcha","recaptcha.js");(function(K,P){void 0!==c[P]&&v("set",K,c[P])})("nonInteraction","vtp_nonInteraction");y("contentGroup",g);y("dimension",h);y("metric",k);var C={};B(e,C,!1)&&v("set",C);var D;c.vtp_enableLinkId&&
v("require","linkid","linkid.js");v("set","hitCallback",function(){var K=e&&e.hitCallback;ha(K)&&K();c.vtp_gtmOnSuccess()});if("TRACK_EVENT"==c.vtp_trackType){c.vtp_enableEcommerce&&(v("require","ec","ec.js"),w());var H={hitType:"event",eventCategory:String(c.vtp_eventCategory||d.category),eventAction:String(c.vtp_eventAction||d.action),eventLabel:x(String,c.vtp_eventLabel||d.label),eventValue:x(qa,c.vtp_eventValue||d.value)};B(D,
H,!1);v("send",H);}else if("TRACK_SOCIAL"==c.vtp_trackType){var N={hitType:"social",socialNetwork:String(c.vtp_socialNetwork),socialAction:String(c.vtp_socialAction),socialTarget:String(c.vtp_socialActionTarget)};B(D,N,!1);v("send",N);}else if("TRACK_TRANSACTION"==c.vtp_trackType){}else if("TRACK_TIMING"==
c.vtp_trackType){}else if("DECORATE_LINK"==c.vtp_trackType){}else if("DECORATE_FORM"==c.vtp_trackType){}else if("TRACK_DATA"==c.vtp_trackType){}else{c.vtp_enableEcommerce&&(v("require","ec","ec.js"),w());if(c.vtp_doubleClick||"DISPLAY_FEATURES"==c.vtp_advertisingFeaturesType){var T=
"_dc_gtm_"+String(c.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");v("require","displayfeatures",void 0,{cookieName:T})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==c.vtp_advertisingFeaturesType){var U="_dc_gtm_"+String(c.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");v("require","adfeatures",{cookieName:U})}D?v("send","pageview",D):v("send","pageview");c.vtp_autoLinkDomains&&Jd(p,c.vtp_autoLinkDomains,!!c.vtp_useHashAutoLink,!!c.vtp_decorateFormsAutoLink);
}if(!a){var Y=c.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";c.vtp_useInternalVersion&&!c.vtp_useDebugVersion&&(Y="internal/"+Y);a=!0;G(E("https:","http:","//www.google-analytics.com/"+Y,e&&e.forceSSL),function(){var K=Gd();K&&K.loaded||c.vtp_gtmOnFailure();},c.vtp_gtmOnFailure)}}else z(c.vtp_gtmOnFailure)};W.__ua=b;W.__ua.g="ua";W.__ua.h=!0;W.__ua.b=0}();



W.a.opt=["google"],function(){var a,b=function(c){var d={};if(c.vtp_gaSettings){var e=c.vtp_gaSettings;Fa(Kh(e.vtp_fieldsToSet,"fieldName","value"),d);c.vtp_gaSettings=null;e.vtp_fieldsToSet=void 0;var g=Fa(e);c=Fa(c,g)||{}}Fa(Kh(c.vtp_fieldsToSet,"fieldName","value"),d);var h=Id(c.vtp_functionName);if(ha(h)){h.r=!0;var k="",l="";c.vtp_setTrackerName&&"string"===typeof c.vtp_trackerName?""!==c.vtp_trackerName&&(l=c.vtp_trackerName,k=l+"."):(l="gtm"+Cc(),k=l+".");var m={name:!0,clientId:!0,sampleRate:!0,
siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},n={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0},p=function(y,w,B){var A=0,C;for(C in y)if(y.hasOwnProperty(C)&&
(B&&m[C]||!B&&void 0===m[C])){var D=n[C]?ra(y[C]):y[C];"anonymizeIp"!==C||D||(D=void 0);w[C]=D;A++}return A},t={name:l};p(d,t,!0);var q={"&gtm":Ig(!0)};p(d,q,!1);var r=encodeURI(E("https:","http:","//www.google-analytics.com/"+(c.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js"),!!d.forceSSL));h("create",c.vtp_trackingId,t);h(k+"set",q);h(k+"require",c.vtp_optimizeContainerId,{dataLayer:"dataLayer"});h(c.vtp_gtmOnSuccess);h(k+"require","render");a||(a=!0,G(r,function(){return Gd().loaded||
c.vtp_gtmOnFailure()},c.vtp_gtmOnFailure));var v=L("dataLayer"),x=v&&v.hide;x&&x.end&&(x[c.vtp_optimizeContainerId]=!0)}else z(c.vtp_gtmOnFailure)};W.__opt=b;W.__opt.g="opt";W.__opt.h=!0;W.__opt.b=0}();
W.a.aev=["google"],function(){var a=void 0,b="",c=0,d=void 0,e={ATTRIBUTE:"gtm.elementAttribute",CLASSES:"gtm.elementClasses",ELEMENT:"gtm.element",ID:"gtm.elementId",HISTORY_CHANGE_SOURCE:"gtm.historyChangeSource",HISTORY_NEW_STATE:"gtm.newHistoryState",HISTORY_NEW_URL_FRAGMENT:"gtm.newUrlFragment",HISTORY_OLD_STATE:"gtm.oldHistoryState",HISTORY_OLD_URL_FRAGMENT:"gtm.oldUrlFragment",TARGET:"gtm.elementTarget"},g=function(m){var n=oh(e[m.vtp_varType],1);return void 0!==n?n:m.vtp_defaultValue},h=function(m,
n){if(!m)return!1;var p=l(lh()),t;t=ka(n.vtp_affiliatedDomains)?n.vtp_affiliatedDomains:String(n.vtp_affiliatedDomains||"").replace(/\s+/g,"").split(",");for(var q=[p],r=0;r<t.length;r++)if(t[r]instanceof RegExp){if(t[r].test(m))return!1}else{var v=t[r];if(0!=v.length){if(0<=l(m).indexOf(v))return!1;q.push(l(v))}}return!Jh(m,q)},k=/^https?:\/\//i,l=function(m){k.test(m)||(m="http://"+m);return ab(bb(m),"HOST",!0)};(function(m){W.__aev=m;W.__aev.g="aev";W.__aev.h=!0;W.__aev.b=0})(function(m){switch(m.vtp_varType){case "TAG_NAME":return oh("gtm.element",
1).tagName||m.vtp_defaultValue;case "TEXT":var n,p=oh("gtm.element",1),t=oh("event",1),q=Number(kh());a===p&&b===t&&c>q-250?n=d:(d=n=p?Sa(p):"",a=p,b=t);c=q;return n||m.vtp_defaultValue;case "URL":var r;a:{var v=String(oh("gtm.elementUrl",1)||m.vtp_defaultValue||""),x=bb(v);switch(m.vtp_component||"URL"){case "URL":r=v;break a;case "IS_OUTBOUND":r=h(v,m);break a;default:r=ab(x,m.vtp_component,m.vtp_stripWww,m.vtp_defaultPages,m.vtp_queryKey)}}return r;case "ATTRIBUTE":var y;if(void 0===m.vtp_attribute)y=
g(m);else{var w=oh("gtm.element",1);y=Qa(w,m.vtp_attribute)||m.vtp_defaultValue||""}return y;default:return g(m)}})}();

W.a.gcs=["google"],function(){(function(a){W.__gcs=a;W.__gcs.g="gcs";W.__gcs.h=!0;W.__gcs.b=0})(function(a){try{var b=a.vtp_siteId;if(!L("_gtmgcs")){f._gtmgcs={};var c=E("https:","http:","//survey.g.doubleclick.net/async_survey?site="+M(b));G(c)}a.vtp_gtmOnSuccess()}catch(d){z(a.vtp_gtmOnFailure)}})}();

W.a.fsl=[],function(){function a(){var e=L("document"),g=c(),h=HTMLFormElement.prototype.submit;Oa(e,"click",function(k){var l=k.target;if(l&&(l=Ua(l,["button","input"],100))&&("submit"==l.type||"image"==l.type)&&l.name&&Qa(l,"value")){var m;(m=l.form?l.form.tagName?l.form:Ra(l.form):Ua(l,["form"],100))&&g.store(m,l)}},!1);Oa(e,"submit",function(k){var l=k.target;if(!l)return k.returnValue;var m=k.defaultPrevented||!1===k.returnValue,n=!0;if(d(l,function(){if(n){var p=g.get(l),t;p&&(t=e.createElement("input"),
t.type="hidden",t.name=p.name,t.value=p.value,l.appendChild(t));h.call(l);t&&l.removeChild(t)}},m,b(l)&&!m))n=!1;else return m||(k.preventDefault&&k.preventDefault(),k.returnValue=!1),!1;return k.returnValue},!1);HTMLFormElement.prototype.submit=function(){var k=this,l=!0;d(k,function(){l&&h.call(k)},!1,b(k))&&(h.call(k),l=!1)}}function b(e){var g=e.target;return g&&"_self"!==g&&"_parent"!==g&&"_top"!==g?!1:!0}function c(){var e=[],g=function(h){return ma(e,function(k){return k.form===h})};return{store:function(h,
k){var l=g(h);l?l.button=k:e.push({form:h,button:k})},get:function(h){var k=g(h);return k?k.button:null}}}function d(e,g,h,k){var l=Af("fsl",h?"nv.mwt":"mwt",0),m;m=h?Af("fsl","nv.ids",[]):Af("fsl","ids",[]);if(!m.length)return!0;var n=wf(e,"gtm.formSubmit",m),p=e.action;p&&p.tagName&&(p=e.cloneNode(!1).action);n["gtm.elementUrl"]=p;if(k&&l){if(!ph(n,g,l))return!1}else ph(n,function(){},l||2E3);return!0}(function(e){W.__fsl=e;W.__fsl.g="fsl";W.__fsl.h=!0;W.__fsl.b=0})(function(e){var g=e.vtp_waitForTags,
h=e.vtp_checkValidation,k=Number(e.vtp_waitForTagsTimeout);if(!k||0>=k)k=2E3;var l=e.vtp_uniqueTriggerId||"0";if(g){var m=function(p){return Math.max(k,p)};zf("fsl","mwt",m,0);h||zf("fsl","nv.mwt",m,0)}var n=function(p){p.push(l);return p};zf("fsl","ids",n,[]);h||zf("fsl","nv.ids",n,[]);wh("fsl")||(a(),xh("fsl"));z(e.vtp_gtmOnSuccess)})}();



W.a.twitter_website_tag=["nonGoogleScripts"],function(){(function(a){W.__twitter_website_tag=a;W.__twitter_website_tag.g="twitter_website_tag";W.__twitter_website_tag.h=!0;W.__twitter_website_tag.b=0})(function(a){(function(c,d){c.twq||(d=c.twq=function(){d.exe?d.exe.apply(d,arguments):d.queue.push(arguments)},d.version="1",d.queue=[],G("//static.ads-twitter.com/uwt.js"))})(window,void 0);window.twq("init",String(a.vtp_twitter_pixel_id));var b=Kh(a.vtp_event_parameters,"param_table_key_column","param_table_value_column");
b&&void 0!==b.content_ids&&(b.content_ids=JSON.parse(b.content_ids.replace(/'/g,'"')));window.twq("track",String(a.vtp_event_type),b);z(a.vtp_gtmOnSuccess)})}();

W.a.html=["customScripts"],function(){function a(d,e,g,h){return function(){try{if(0<e.length){var k=e.shift(),l=a(d,e,g,h);if("SCRIPT"==String(k.nodeName).toUpperCase()&&"text/gtmscript"==k.type){var m=u.createElement("script");m.async=!1;m.type="text/javascript";m.id=k.id;m.text=k.text||k.textContent||k.innerHTML||"";k.charset&&(m.charset=k.charset);var n=k.getAttribute("data-gtmsrc");n&&(m.src=n,Ja(m,l));d.insertBefore(m,null);n||l()}else if(k.innerHTML&&0<=k.innerHTML.toLowerCase().indexOf("<script")){for(var p=
[];k.firstChild;)p.push(k.removeChild(k.firstChild));d.insertBefore(k,null);a(k,p,l,h)()}else d.insertBefore(k,null),l()}else g()}catch(t){z(h)}}}var c=function(d){if(u.body){var e=
d.vtp_gtmOnFailure,g=Ah(d.vtp_html,d.vtp_gtmOnSuccess,e),h=g.nc,k=g.I;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(u.body,Ta(h),k,e)()}else jh(function(){c(d)},
200)};W.__html=c;W.__html.g="html";W.__html.h=!0;W.__html.b=0}();


W.a.lcl=[],function(){function a(){var c=L("document"),d=0,e=function(g){var h=g.target;if(h&&3!==g.which&&(!g.timeStamp||g.timeStamp!==d)){d=g.timeStamp;h=Ua(h,["a","area"],100);if(!h)return g.returnValue;var k=g.defaultPrevented||!1===g.returnValue,l=Af("lcl",k?"nv.mwt":"mwt",0),m;m=k?Af("lcl","nv.ids",[]):Af("lcl","ids",[]);if(m.length){var n=wf(h,"gtm.linkClick",m);if(b(g,h,c)&&!k&&l&&h.href){var p=L((h.target||"_self").substring(1)),t=!0;if(ph(n,function(){t&&p&&(p.location.href=h.href)},l))t=
!1;else return g.preventDefault&&g.preventDefault(),g.returnValue=!1}else ph(n,function(){},l||2E3);return!0}}};Oa(c,"click",e,!1);Oa(c,"auxclick",e,!1)}function b(c,d,e){if(2===c.which||c.ctrlKey||c.shiftKey||c.altKey||c.metaKey)return!1;var g=d.href.indexOf("#"),h=d.target;if(h&&"_self"!==h&&"_parent"!==h&&"_top"!==h||0===g)return!1;if(0<g){var k=nh(d.href),l=nh(e.location);return k!==l}return!0}(function(c){W.__lcl=c;W.__lcl.g="lcl";W.__lcl.h=!0;W.__lcl.b=0})(function(c){var d=void 0===c.vtp_waitForTags?
!0:c.vtp_waitForTags,e=void 0===c.vtp_checkValidation?!0:c.vtp_checkValidation,g=Number(c.vtp_waitForTagsTimeout);if(!g||0>=g)g=2E3;var h=c.vtp_uniqueTriggerId||"0";if(d){var k=function(m){return Math.max(g,m)};zf("lcl","mwt",k,0);e||zf("lcl","nv.mwt",k,0)}var l=function(m){m.push(h);return m};zf("lcl","ids",l,[]);e||zf("lcl","nv.ids",l,[]);wh("lcl")||(a(),xh("lcl"));z(c.vtp_gtmOnSuccess)})}();

var aj={};aj.macro=function(a){if(tf.$b.hasOwnProperty(a))return tf.$b[a]},aj.onHtmlSuccess=tf.td(!0),aj.onHtmlFailure=tf.td(!1);aj.dataLayer=gd;aj.callback=function(a){Ac.hasOwnProperty(a)&&ha(Ac[a])&&Ac[a]();delete Ac[a]};aj.Ve=function(){sc[rc.m]=aj;Bc=W.a;Rb=Rb||tf;Sb=sd};
aj.Jf=function(){sc=f.google_tag_manager=f.google_tag_manager||{};if(sc[rc.m]){var a=sc.zones;a&&a.unregisterChild(rc.m)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)Kb.push(c[d]);for(var e=b.tags||[],g=0;g<e.length;g++)Nb.push(e[g]);for(var h=b.predicates||[],k=0;k<h.length;k++)Mb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var n=l[m],p={},t=0;t<
n.length;t++)p[n[t][0]]=Array.prototype.slice.call(n[t],1);Lb.push(p)}Pb=W;Qb=Dh;aj.Ve();Re();vd=!1;wd=0;if("interactive"==u.readyState&&!u.createEventObject||"complete"==u.readyState)yd();else{Oa(u,"DOMContentLoaded",yd);Oa(u,"readystatechange",yd);if(u.createEventObject&&u.documentElement.doScroll){var q=!0;try{q=!f.frameElement}catch(y){}q&&zd()}Oa(f,"load",yd)}Fe=!1;"complete"===u.readyState?He():Oa(f,"load",He);a:{if(!Xc)break a;f.setInterval(Yc,864E5);}xc=(new Date).getTime();}};(0,aj.Jf)();

})()
