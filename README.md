# Instructions

The project consists of 2 folders: [implementation](implementation) and [input](input).

In the [implementation](implementation) folder are our algorithms for regex, xpath and roadrunner. 
The input folder contains the folders with content of choosen websites (rtvslo, overstock, theverge).

The implementation folder consists of [results](implementation/results) and [pages](implementation/pages) .

# Setup
In order to set up the project we need to move to the implementation folder and install node packages. 
We were using node version 10.15.1 (if there are problems with running regex.js file you might need to upgrade node version).
```bash
cd implementation
npm install
```

# Run the Algorithms
To run particular extraction of the websites run the files with the sitename proveded as an argument. 
```bash
# run regex
node regex.js rtvslo.si 
node regex.js theverge.com
node regex.js overstock.com
```

```bash
# run xpath
node xpath.js rtvslo.si 
node xpath.js theverge.com
node xpath.js overstock.com
```

```bash
# run roadrunner
node roadrunner.js rtvslo.si 
node roadrunner.js theverge.com
node roadrunner.js overstock.com
```


The results of running the algoritms is printed on the standard output.


All the results in json format are also available in [results](implementation/results) folder.